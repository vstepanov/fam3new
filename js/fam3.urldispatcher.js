!function() {
  var urldispatcher = {
    init: function() {
      $(window).hashchange(handler);
      $(window).hashchange();
    }
  };
  
  function handler() {
    var self = fam3.urldispatcher;
    self.link = window.location.hash.replace("#", "");
  }
  
  fam3.urldispatcher = urldispatcher;
}();



$(function() {
  fam3.urldispatcher.init();
});