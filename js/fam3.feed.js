!function() {
  var feed = {
    init: function() {
      $(document).on('focus', '#write_post textarea', function() {
        $('#write_post').addClass('expanded');
      });
      $(document).on('change', '#write_post select', function() {
        if($(this).val() == feed.const.PRIVACY_PRIVATE)
          $('#write_post').addClass('private');
        else
          $('#write_post').removeClass('private');
      });
    },
    const: {
      PRIVACY_PUBLIC:  0,
      PRIVACY_PRIVATE: 1
    }
  };
  
  fam3.feed = feed;
}();



$(function() {
  fam3.feed.init();
});