<?php

interface ITreeOperated
{
  public function getTreeId();
  
  public function setTreeId($treeId);
  
  public function hasTheSameTree($comparedObject);
}