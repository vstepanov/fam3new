<?php

interface IGenderOperated
{
  public function isMale();
  
  public function isFemale();
  
  public function getGender();
  
  public function setGender($gender);
}