<?php

class SigninForm extends CFormModel
{
  public $email;
	public $password;
	
	private $_identity;
	
	public function rules()
  {
    return array(
      array('email, password', 'required'),
      array('email', 'email'),
      array('email', 'authenticate'),
    );
  }
  
  public function attributeLabels()
	{
		return array(
		  'email' => 'Email',
		  'password' => 'Пароль',
		);
	}
  
  public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity = new UserIdentity($this->email, $this->password);
			if($this->_identity->authenticate())
			  Yii::app()->user->login($this->_identity);
			else
				$this->addError('email', 'Неправильный e-mail или пароль');
		}
	}
}