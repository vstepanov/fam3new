<?php

class FeedReader
{
  private $_profile;
  private $_feeds = array();
  private $_type;
  private $_contractors = array(); // массив вида person_id => Profile
 
  const TYPE_ALL      = 'all';      // показывать все сообщения пользователя
  const TYPE_SELF     = 'self';     // показывать только публичные сообщения отправленные пользователем
  const TYPE_PUBLIC   = 'public';   // показывать только публичные сообщения отправленные или адресованные пользователю
  const TYPE_PRIVATE  = 'private';  // показывать только личные сообщения отправленные или адресованные пользователю
  const TYPE_INDEX    = 'index';    // показывать все публичные сообщения родственников
  
  public function __construct(Profile $profile, $type = null)
  {
    $this->_profile = $profile;
    $this->setType($type);
  }
  
  public function setType($type)
  {
    if(is_null($type))
      $type = self::TYPE_PUBLIC;

    if(!$this->_profile->hasTheSameTree(User::instance()->getProfile()))
      throw new Fam3AccessDeniedException('Вы не можете читать ленту');
    
    switch($type)
    {
      case self::TYPE_ALL:
      case self::TYPE_SELF:
      case self::TYPE_PUBLIC:
      case self::TYPE_PRIVATE:
      case self::TYPE_INDEX:
        $this->_type = $type;
        break;
      default:
        throw new Fam3InternalException('FeedReader type ' . $type . ' not defined');
    }
  }
  
  public function getTypeName($type)
  {
    switch($type)
    {
      case self::TYPE_ALL:
        return 'Все';
      case self::TYPE_PRIVATE:
        return 'Личные';
        
      case self::TYPE_PUBLIC:
        return 'Общие';
        
      /*case self::TYPE_SELF:
        return 'Исходящие';*/
        
      case self::TYPE_PUBLIC:
        if($this->_profile->isMy())
          return 'Мне написали';
        elseif(Gender::isFemale($this->_profile->getGender()))
          return 'Ей написали';
        else
          return 'Ему написали';
      case self::TYPE_SELF:
        if($this->_profile->isMy())
          return 'Я написал';
        elseif(Gender::isFemale($this->_profile->getGender()))
          return 'Она написала';
        else
          return 'Он написал';
    }
  }
  
  public function load($limit = 50, $offset = 0)
  {
    if(Yii::app()->user->isGuest || !$this->_profile->getRelated('person')->hasTheSameTree(User::instance()->getRelated('person')))
      return;

    $where = '';
    
    switch($this->_type)
    {
      case self::TYPE_INDEX:
        $where .= 'pea.tree_id = ' . User::instance()->getPerson()->getTreeId() . ' and per.tree_id = ' . User::instance()->getPerson()->getTreeId() . ' and f.privacy = ' . Feed::PRIVACY_PUBLIC;// . ' or (f.recipient_id = ' . Yii::app()->user->getProfileId() . ' or f.author_id = ' . Yii::app()->user->getProfileId() . ') and f.privacy = ' . Feed::PRIVACY_PRIVATE;
        break;
      case self::TYPE_ALL:
        $where .= '(f.recipient_id = ' . $this->_profile->getPrimaryKey() . ' or f.author_id = ' . $this->_profile->getPrimaryKey() . ') and f.privacy = ' . Feed::PRIVACY_PUBLIC . ' or ((f.recipient_id = ' . Yii::app()->user->getProfileId() . ' or f.author_id = ' . Yii::app()->user->getProfileId() . ') and (f.recipient_id = ' . $this->_profile->getPrimaryKey() . ' or f.author_id = ' . $this->_profile->getPrimaryKey() . ') and f.privacy = ' . Feed::PRIVACY_PRIVATE . ')';
        break;
      case self::TYPE_PUBLIC:
        $where .= 'f.recipient_id = ' . $this->_profile->getPrimaryKey() . ' and f.privacy = ' . Feed::PRIVACY_PUBLIC . ($this->_profile->isMy() ? ' and f.system_type is null' : '');
        break;
      case self::TYPE_SELF:
        $where .= 'f.author_id = ' . $this->_profile->getPrimaryKey() . ' and f.privacy = ' . Feed::PRIVACY_PUBLIC;
        break;
      case self::TYPE_PRIVATE:
        $where .= '(f.recipient_id = ' . Yii::app()->user->getProfileId() . ' or f.author_id = ' . Yii::app()->user->getProfileId() . ') and f.privacy = ' . Feed::PRIVACY_PRIVATE . ' and pra.user_id is not null and prr.user_id is not null';
        if(!$this->_profile->isMy())
          $where .= ' and (f.recipient_id = ' . $this->_profile->getPrimaryKey() . ' or f.author_id = ' . $this->_profile->getPrimaryKey() . ')';
        break;
    }
    $where .= ($where ? ' and ' : '') . 'f.is_active = 1 ';

    $sql = '
select
	f.id,
	f.author_id,
	f.parent_id,
	f.recipient_id,
	f.body,
	f.privacy,
	f.system_type,
	f.system_value,
	f.created_at,
	pra.name a_name,
	pra.surname a_surname,
	pra.middlename a_middlename,
	pra.person_id a_person_id,
	pra.user_id a_user_id,
	pea.gender a_gender,
	pea.tree_id a_tree_id,
	prr.name r_name,
	prr.surname r_surname,
	prr.middlename r_middlename,
	prr.person_id r_person_id,
	prr.user_id r_user_id,
	per.gender r_gender,
	per.tree_id r_tree_id
	' . ($this->_type == FeedReader::TYPE_PRIVATE && $this->_profile->isMy() ? ',if(f.recipient_id = ' . Yii::app()->user->getProfileId() . ', f.author_id, f.recipient_id) as contractor' : '') . '
from feed f
left join profile pra on (pra.id = f.author_id)
left join person pea on (pea.profile_id = pra.id)
left join profile prr on (prr.id = f.recipient_id)
left join person per on (per.profile_id = prr.id)
where ' . $where . '
order by f.created_at desc';
    if($this->_type == FeedReader::TYPE_PRIVATE && $this->_profile->isMy())
      $sql = 'select t.* from (' . $sql . ') t group by contractor';
    $sql .= ' limit ' . $offset . ', ' . $limit;
    $command = Feed::model()->dbConnection->createCommand($sql);
    $dataReader = $command->query();
    $dataReader->setFetchMode(PDO::FETCH_NUM);
    while(($row = $dataReader->read()) !== false)
  	{
    	$feed = new Feed();
    	$feed->setAttributes(array(
    	  'id' => $row[0],
    	  'author_id' => $row[1],
    	  'parent_id' => $row[2],
    	  'recipient_id' => $row[3],
    	  'body' => $row[4],
    	  'privacy' => $row[5],
    	  'system_type' => $row[6],
    	  'system_value' => $row[7],
    	  'created_at' => $row[8],
    	), false);
    	$feed->author = Profile::create(array(
    	  'id' => $row[1],
    	  'name' => $row[9],
    	  'surname' => $row[10],
    	  'middlename' => $row[11],
    	  'person_id' => $row[12],
    	  'user_id' => $row[13],
    	), $row[14], $row[15]);
    	$feed->recipient = Profile::create(array(
    	  'id' => $row[3],
    	  'name' => $row[16],
    	  'surname' => $row[17],
    	  'middlename' => $row[18],
    	  'person_id' => $row[19],
    	  'user_id' => $row[20],
    	), $row[21], $row[22]);
    	if($this->_type == FeedReader::TYPE_PRIVATE && $this->_profile->isMy())
    	{
    	  $feed->setContractorId($row[23]);
    	  $contractorPersonId = $row[23] == $row[1] ? $row[12] : $row[19];
    	  $this->_contractors[$contractorPersonId] = $row[23] == $row[1] ? $feed->author : $feed->recipient;
    	}
    	$this->_feeds[] = $feed;
  	}
  }
  
  public function filterIs($filter)
  {
    return $this->_filter == $filter;
  }
  
  public function typeIs($type)
  {
    return $this->_type == $type;
  }
  
  public function isPrivate()
  {
    return $this->_type == FeedReader::TYPE_PRIVATE;
  }
  
  public function isPublic()
  {
    return $this->_type == FeedReader::TYPE_PUBLIC;
  }
  
  public function getFeeds()
  {
    return $this->_feeds;
  }
  
  public function getCount()
  {
    return count($this->_feeds);
  }
  
  public function getContractors()
  {
    return $this->_contractors;
  }
}