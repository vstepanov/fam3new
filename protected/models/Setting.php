<?php

class Setting extends Model
{
  const PRIVACY_PUBLIC      = 10;
  const PRIVACY_REGISTERED  = 20;
  const PRIVACY_RELATIVES   = 30;
  const PRIVACY_PRIVATE     = 40;
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
    
	public function tableName()
	{
		return 'setting';
	}
}