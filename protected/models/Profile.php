<?php

class Profile extends Model implements IGenderOperated, ITreeOperated
{
  const ALL_RELATIONS = null;
  const FORCE_LOAD_RELATIVES = true;
  const HAVING_USERS = true;
  
  private static $_instance = null; // profile of current page
  private $_relatives = array();
  private $_loadedRelations = array(); // список загруженных путей
  private $_gender = null;
  private $_treeId = null;
  private $_birthd = null; // испольузется в методах getBirthd, setBirthd - нужно для работы валидатора date
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  
	public function tableName()
	{
		return 'profile';
	}
	
	public function rules()
	{
		return array(
		  array('name, surname, middlename, maiden_name' , 'length', 'max' => 30, 'allowEmpty' => true),
			array('birth_country, birth_place, death_place, death_country, live_country, live_place, profession' , 'length', 'max' => 100, 'allowEmpty' => true),
			array('birthd', 'date', 'format' => 'd.M.yyyy', 'allowEmpty' => true),
			array('photo', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true),
			array('about', 'length', 'max' => 65000, 'allowEmpty' => true),
		);
	}
	
	public function attributeLabels()
	{
		return array(
		  'name' => 'Имя',
		  'middlename' => 'Отчество',
		  'surname' => 'Фамилия',
		  'maiden_name' => 'Девичья фамилия',
		  'birth_country' => 'Страна рождения',
		  'birth_place' => 'Место рождения',
		  'birthdate' => 'Дата рождения',
		  'birthd' => 'Дата рождения',
		  'gender' => 'Пол',
		);
	}
	
	public function relations()
	{
  	return array(
  	  'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
  	);
	}
	
	public static function url($profileId, $tab = null, $params = null)
	{
	  if(is_null($params))
	    $params = array();
	  if($tab == 'feed')
	  {
  	  if(!isset($params['type']) || $params['type'] == FeedReader::TYPE_PUBLIC)
  	  {
    	  unset($params['type']);
    	  $tab = null;
      }
        
	  }
    return Yii::app()->createUrl('profile/' . (is_null($tab) ? 'index' : $tab), array('profileId' => $profileId) + $params);
	}
	
	public function getLink($tab = null, $params = null)
	{
    return self::url($this->id, $tab, $params);
	}
	
	public function customLink($url, $htmlParams = null, $avatarPosition = false)
	{
  	return $this->link($htmlParams, null, null, $avatarPosition, $url);
	}
	
	public function link($htmlParams = null, $tab = null, $params = null, $avatarPosition = false /* left or right allowed */, $url = null)
	{
	  if(!is_array($htmlParams))
	    $htmlParams = array();
	  $cssClass = $avatarPosition ? '' : 'profile';
	  if($this->isMy())
	    $cssClass .= ' is_my';
	  if(self::$_instance && self::$_instance->getPrimaryKey() == $this->getPrimaryKey())
	    $cssClass .= ' is_current';
	  if($this->hasUser())
	    $cssClass .= ' has_user';
	  if($cssClass)
	  {
	    if(!isset($htmlParams['class']))
	      $htmlParams['class'] = '';
	    $htmlParams['class'] .= $cssClass;
	  }
	  if($avatarPosition == 'left')
	    return '<span class="profile">' . $this->avatar() . ' ' . CHtml::link($this->displayName(), is_null($url) ? $this->getLink($tab, $params) : $url, $htmlParams) . '</span>';
	  elseif($avatarPosition == 'right')
	    return '<span class="profile">' . CHtml::link($this->displayName(), is_null($url) ? $this->getLink($tab, $params) : $url, $htmlParams) . ' ' . $this->avatar() . '</span>';
	  else
	    return CHtml::link($this->displayName(), is_null($url) ? $this->getLink($tab, $params) : $url, $htmlParams);
	}
	
	public function avatar()
	{
  	return '<img src="/img/noavatar.gif" style="width:25px;height:25px;" class="avatar">';
	}
	
	/**
	 * Shortcut for link $withAvatar = left
	 */
	public function alink($htmlParams = null, $tab = null, $params = null)
	{
  	return $this->link($htmlParams, $tab, $params, 'left');
	}
	
	/**
	 * Shortcut for link $withAvatar = right
	 */
	public function arlink($htmlParams = null, $tab = null, $params = null)
	{
  	return $this->link($htmlParams, $tab, $params, 'right');
	}
	
	public function displayName()
	{
	  $displayName = '';
  	if($this->getAttribute('name'))
  	  $displayName .= $this->getAttribute('name');
    /*if($this->getAttribute('middlename') && $displayName)
  	  $displayName .= ' ' . $this->getAttribute('middlename');*/
    if($this->getAttribute('surname'))
  	  $displayName .= ($displayName ? ' ' : '') . $this->getAttribute('surname');
  	return $displayName ? $displayName : ($this->isMy() ? 'Ваше Имя' : 'Имя неизвестно');
	}
	
	public function isMy()
	{
  	return !Yii::app()->user->isGuest && $this->getPrimaryKey() == Yii::app()->user->getProfileId();
  	//return $this->getRelated('person')->getAttribute('profile_id') == $this->getPrimaryKey() && $this->getRelated('person')->isMy();
	}
	
	/**
   * Retrieve current Profile object of page
   *
   * @param Profile || int || null $profile
   * @return Profile || null
   */
  public static function instance($profile = null)
  {
    if(!is_null($profile))
    {
      if($profile instanceof Profile)
        self::$_instance = $profile;
      elseif(!is_null(self::$_instance) && $profile != self::$_instance->getPrimaryKey())
        throw new Fam3InternalException('Trying re-init Profile instance');
      else
        self::$_instance = self::retrieve($profile);
    }
    elseif(is_null(self::$_instance))
      throw new Fam3InternalException('Trying retrieve empty Profile instance');
    return self::$_instance;
  }
  
  public static function hasInstance()
  {
    return !is_null(self::$_instance);
  }
  
  public static function retrieve($profileId)
  {
    return Profile::model()->with('person')->with('person.user')->with('person.user.setting')->findByPk($profileId);
  }
  
  public static function create($attributes, $gender, $treeId = null)
  {
    $profile = new Profile;
    $profile->setAttributes($attributes, false);
    $profile->setGender($gender);
    $profile->setTreeId($treeId);
    return $profile;
  }
	
	public function getBirthdate()
	{
    if(!($this->birth_day && $this->birth_month && $this->birth_year))
      return '';
    return sprintf("%02d", $this->birth_day) . '.' . sprintf("%02d", $this->birth_month) . '.' . $this->birth_year;
	}
	
	public function setBirthdate($birthdate)
	{
	  if(!$birthdate)
	    return;
	  $date = DateTime::createFromFormat('j.n.Y', $birthdate);
	  if(!$date)
	    return;
	  $this->setAttribute('birth_day', $date->format('j'));
	  $this->setAttribute('birth_month', $date->format('n'));
	  $this->setAttribute('birth_year', $date->format('Y'));
	}
	
	public function setBirthd($birthdate)
	{
  	$this->_birthd = $birthdate;
  	$this->setBirthdate($birthdate);
	}
	
	public function getBirthd()
	{
	  if(is_null($this->_birthd))
	    return $this->getBirthdate();
  	return $this->_birthd;
	}
	
	/**
	 * @return array
	 */
	public function getChilds()
	{
  	return $this->getRelatives(Relation::CHILD, Profile::FORCE_LOAD_RELATIVES);
	}
	
	/**
	 * @return array
	 */
	public function getParents()
	{
  	return $this->getRelatives(Relation::PARENT, Profile::FORCE_LOAD_RELATIVES);
	}
	
  /**
	 * @return Profile || null
	 */
	public function getParent($gender)
	{
  	Gender::validate($gender);
  	foreach($this->getParents() AS $parent)
    {
      if($parent->getGender() == $gender)
        return $parent;
    }
    return null;
	}
	
	public function hasBothParents()
	{
  	return count($this->getParents()) == 2;
	}
	
	public function hasParent($gender)
	{
	  return $this->getParent($gender);
	}
	
	public function hasChilds()
	{
  	return $this->getChilds();
	}
	
	/**
	 * Get specified or all relatives
	 *
	 * @param int || null $relation
	 * @param boolean $force - force load relations before
	 * @return array
	 */
	public function getRelatives($relation = null, $force = false, $havingUsers = null)
	{
	  if(is_null($relation))
	  {
  	  if($force)
  	    $this->loadRelatives(null, $havingUsers);
  	  return $this->_relatives;   
	  }  
    else
    {
      if($force && !$this->isLoadedRelatives($relation))
        $this->loadRelatives($relation, $havingUsers);
      if($this->hasRelatives($relation))
    	  return $this->_relatives[$relation];
      else
        return array();
    }
	}
	
	/**
	 * Загружает родствеников и возвращает кол-во загруженных элементов
	 *
	 * @param array || int $relations - список типов родственников, которых стоит загрузить
	 * @return int
	 */
	public function loadRelatives($relations = null, $havingUsers = null)
	{
	  if(is_null($relations))
	    $relations = array_keys(Relation::getRelations());
	  elseif(!is_array($relations))
	    $relations = array($relations);
	  $relations = array_diff($relations, $this->_loadedRelations);
	  if(count($relations) == 0)
	    return 0;
	  $this->_loadedRelations = array_merge($this->_loadedRelations, $relations);
  	$sql = '
select
	r.path, pr.id, pr.name, pr.surname, pr.middlename, pr.user_id, pe.id, pe.gender
from ' . Relation::tableName() . ' r
left join ' . Person::tableName() . ' pe on (pe.id = r.person2_id)
left join ' . Profile::tableName() . ' pr on (pr.id = pe.profile_id)
where r.person1_id = :person_id and r.path in ('.implode(',', $relations).')';
    if($havingUsers)
      $sql .= ' and pr.user_id is not null';
    $command = Profile::model()->dbConnection->createCommand($sql);
  	$command->bindParam(":person_id", $this->getAttribute('person_id'), PDO::PARAM_INT);
  	$dataReader = $command->query();
  	$dataReader->setFetchMode(PDO::FETCH_NUM);
  	$count = 0;
  	while(($profile = $dataReader->read()) !== false)
  	{
  	  $path = $profile[0];
  	  $id = $profile[1];
  	  $count++;
    	if(!isset($this->_relatives[$path]))
    	  $this->_relatives[$path] = array();
    	$this->_relatives[$path][$id] = Profile::create(array(
    	  'id' => $id,
    	  'name' => $profile[2],
    	  'surname' => $profile[3],
    	  'middlename' => $profile[4],
    	  'user_id' => $profile[5],
    	  'person_id' => $profile[6],
    	), $profile[7]);
  	}
  	ksort($this->_relatives);
  	return $count;
	}
	
	public function isLoadedRelatives($relation)
	{
  	return isset($this->_loadedRelations[$relation]);
	}
	
	public function hasRelatives($relation)
	{
  	return isset($this->_relatives[$relation]);
	}
	
	/**
	 * Search and return path from $this to $relatedProfile
	 *
	 * @param Profile $relatedProfile
	 * @return int || null
	 */
	public function getPath(Profile $relatedProfile)
	{
	  $id = $relatedProfile->getPrimaryKey();
  	foreach($this->_relatives AS $path => $profiles)
  	{
    	if(isset($profiles[$id]))
    	  return $path;
  	}
  	return null;
	}
	
	public function validate()
	{
  	if(!parent::validate())
  	  return false;
    if($this->_gender && $this->_gender != $this->getRelated('person')->getAttribute('gender') && $this->hasChilds())
      throw new Fam3InternalException('It\'s impossible to change gender of person have childs');
    else
      return true;
	}
  
  public function hasUser()
  {
    return $this->getAttribute('user_id');
  }
  
  public function hasAnotherUser()
  {
    return Yii::app()->user->isGuest || ($this->getAttribute('user_id') && $this->getAttribute('user_id') != Yii::app()->user->getId());
  }
  
  // Implementation ITreeOperated begin:
  
  public function hasTheSameTree(/*Profile*/ $profile)
	{
  	return $this->getTreeId() == $profile->getTreeId();
	}
	
	public function getTreeId()
  {
    if(!is_null($this->_treeId))
	    return $this->_treeId;
	  if(!$this->hasRelated('person'))
	    throw new Fam3InternalException('Can\'t get tree_id because it\'s not init and profile->person isn\'t load');
  	return $this->getRelated('person')->getAttribute('tree_id');
  }
  
  public function setTreeId($treeId)
  {
    if($this->hasRelated('person'))
	    $this->getRelated('person')->setAttribute('tree_id', $treeId);
  	$this->_treeId = $treeId;
  }
  
  // implementation end
  
  
  // Implementation IGenderOperated begin:
  
  public function isMale()
  {
    return Gender::isMale($this->getGender(false));
  }
  
  public function isFemale()
  {
    return Gender::isFemale($this->getGender(false));
  }
  
  public function getGender($exception = true)
	{
	  if(!is_null($this->_gender))
	    return $this->_gender;
	  if(!$this->hasRelated('person'))
	  {
  	  if($exception)
  	    throw new Fam3InternalException('Can\'t get gender because it\'s not init and profile->person isn\'t load');
  	  else
  	    return false;
	  }
  	return $this->getRelated('person')->getAttribute('gender');
	}
	
	public function setGender($gender)
	{
	  if($this->hasRelated('person'))
	    $this->getRelated('person')->setAttribute('gender', $gender);
  	$this->_gender = $gender;
	}
	
  // implementation end
  
  
  protected function _searchChanges($oldAttributes)
  {
    foreach($this->getAttributes() as $k => $v)
    {
      if((!isset($oldAttributes[$k]) && $v) || (isset($oldAttributes[$k]) && $oldAttributes[$k] != $v))
        $this->_changes[$k] = array($oldAttributes[$k], $v);
    }
  }
  
  protected function afterFind()
	{
	  if($this->hasRelated('person'))
      $this->_gender = $this->getRelated('person')->getAttribute('gender');
  }
	
}