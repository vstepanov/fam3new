<?php

Yii::import('application.vendors.urllinker.*');

require_once('UrlLinker.php');

class Feed extends Model
{
  const PRIVACY_PUBLIC    = 0;
  const PRIVACY_PRIVATE   = 1;
  
  const SYSTEM_TYPE_PROFILE_EDIT = 10;
  const SYSTEM_TYPE_PROFILE_ADD  = 11;
  
  private $_contractorId; // контрагент в личной переписке
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
    
	public function tableName()
	{
		return 'feed';
	}
	
	public function rules()
	{
		return array(
		  array('body', 'required'),
			array('body' , 'length', 'max' => 10000, 'allowEmpty' => false),
			array('privacy', 'in', 'range' => array(Feed::PRIVACY_PUBLIC, Feed::PRIVACY_PRIVATE), 'allowEmpty' => true),
			array('privacy', 'validatePrivacy'),
		);
	}
	
	public function relations()
	{
  	return array(
  	  'author' => array(self::BELONGS_TO, 'Profile', 'author_id'),
  	  'recipient' => array(self::BELONGS_TO, 'Profile', 'recipient_id'),
  	);
	}
	
	public function validatePrivacy($attribute, $params)
  {
    if($this->isPrivate() && !$this->getRelated('recipient')->hasUser())
      throw new Fam3InternalException('Private messages can only be sent to users');
  }
	
	public function isSystem()
	{
  	return $this->getAttribute('system_type');
	}
	
	public function isHimself()
	{
  	return $this->getAttribute('author_id') == $this->getAttribute('recipient_id');
	}
	
	public function setContractorId($contractorId)
	{
  	$this->_contractorId = $contractorId;
	}
	
	public function setRecipient(Profile $recipient)
	{
  	$this->setAttribute('recipient_id', $recipient->getPrimaryKey());
  	$this->recipient = $recipient;
	}
	
	/**
	 * Contractor - это собеседник текущего пользователя
	 */
	public function getContractor()
	{
  	return $this->getRelated($this->_contractorId == $this->getAttribute('author_id') ? 'author' : 'recipient');
	}
	
	public function isPublic()
	{
  	return $this->getAttribute('privacy') == Feed::PRIVACY_PUBLIC;
	}
	
	public function isPrivate()
	{
  	return $this->getAttribute('privacy') == Feed::PRIVACY_PRIVATE;
	}
	
	/**
	 * @return string
	 */
	public function renderBody()
	{
  	if($this->isSystem())
    {
      switch($this->getAttribute('system_type'))
      {
        case Feed::SYSTEM_TYPE_PROFILE_EDIT:
          return Yii::app()->getController()->renderPartial('//feed/_systemProfileEdit', array('feed' => $this, 'changes' => unserialize($this->getAttribute('body'))), true);
        case Feed::SYSTEM_TYPE_PROFILE_ADD:
          return 'Создал профиль';
      }
    }
    else
      return nl2br(preg_replace("|(\\n\s*){3,}|s","\n\n",UrlLinker::htmlEscapeAndLinkUrls(trim($this->getAttribute('body')))));
      //return nl2br(htmlspecialchars(trim($this->getAttribute('body'))));
	}
	
	/**
	 * Создает системный фид типа SYSTEM_TYPE_PROFILE_EDIT (изменение профиля)
	 *
	 * @param array $changes Изменения в профиле (включая gender)
	 * @param int $editedProfileId Id измененного профиля
	 * @return Feed
	 */
	public static function createSystemProfileEdit(array $changes, $editedProfileId)
	{
  	return self::_createSystemFeed(Feed::SYSTEM_TYPE_PROFILE_EDIT, $editedProfileId, serialize($changes));
	}
	
	public static function createSystemProfileAdd($addedProfileId)
	{
  	return self::_createSystemFeed(Feed::SYSTEM_TYPE_PROFILE_ADD, $addedProfileId);
	}
	
	private static function _createSystemFeed($type, $recipientId, $body = null, $systemValue = null, $privacy = Feed::PRIVACY_PUBLIC)
	{
  	$feed = new Feed('insert');
  	$attrs = array(
  	  'author_id' => Yii::app()->user->getProfileId(),
  	  'recipient_id' => $recipientId,
  	  'privacy' => $privacy,
  	  'system_type' => $type
  	);
  	if(!is_null($body))
  	  $attrs['body'] = $body;
    if(!is_null($systemValue))
  	  $attrs['system_value'] = $systemValue;
  	$feed->setAttributes($attrs, false);
  	$feed->save(false, array_keys($attrs));
  	return $feed;
	}
}