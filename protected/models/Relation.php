<?php

class Relation extends Model
{  
  // отношения 1-го уровня
  const PARENT    = 2;
  const CHILD     = 3;
  
  // отношения 2-го уровня
  const SIBLING   = 5;
  const SPOUSE    = 6;
  
  private static $_relationsBetween = array();
 
  private static $_relations = array(
 // relation of gender1 -> gender2
 // path => array( [null null]_str,[null null]_def,  [m null]_str,[m null]_def,  [f null]_str,[f null]_def )
 // path => array( [null null]_str,[null null]_def,  [m m]_str,[m m]_def,  [f m]_str,[f m]_def,  [m f]_str,[m f]_def,  [f f]_str,[f f]_def )
  
/*0b10 2*/ 2  => array( 'родители',null,                       'отец',null,               'мать',null ),
/*0b11 3*/ 3  => array( 'дети',null,                           'сын',null,                'дочь',null ),
    
/*0b100*/  4  => array( 'дедушки/бабушки',null,                'дедушка',null,            'бабушка',null ),
/*0b101*/  5  => array( 'братья/сестры',null,                  'брат',null,               'сестра',null ),
/*0b110*/  6  => array( 'супруги',null,                        'муж',null,                'жена',null ),
/*0b111*/  7  => array( 'внуки',null,                          'внук',null,               'внучка',null ),
    
/*0b1000*/ 8  => array( 'прадедушки/прабабушки',null,          'прадедушка',null,         'прабабушка',null ),
/*0b1001*/ 9  => array( 'дяди/тети',null,                      'дядя',null,               'тетя',null ),
/*0b1011*/ 11 => array( 'племянники',null,                     'племянник',null,          'племянница',null ),
/*0b1100*/ 12 => array( 'родители супругов','тести/свекры',    'отец жены','тесть',       'мать жены','теща',  'отец мужа','свекр',  'мать мужа','свекровь' ),
/*0b1110*/ 14 => array( 'супруги детей','зятья/невестки',      'муж дочери','зять',       'жена сына','сноха', 'муж дочеры','зять',  'жена сына','невестка' ),
/*0b1111*/ 15 => array( 'правнуки',null,                       'правнук',null,            'правнучка',null ),
    
/*0b10000*/16 => array( 'прапрадедушки/бабушки',null,          'прапрадедушка',null,       'прапрабабушка',null ),
/*0b10001*/17 => array( 'двоюродные дедушки/бабушки',null,     'двоюродный дедушка',null,  'двоюродная бабушка',null ),
/*0b10011*/19 => array( 'двоюродные братья/сестры',null,       'двоюродный брат',null,     'двоюродная сестра',null ),
/*0b10110*/22 => array( 'супруги братьев/сестер',null,         'муж сестры','зять',        'жена брата','золовка' ),
/*0b10111*/23 => array( 'внучатые племянники',null,            'внучатый племянник',null,  'внучатая племянница',null ),
/*0b11001*/25 => array( 'братья/сестры супругов','шурины/свояченицы', 'брат жены','шурин', 'сестра жены','свояченица', 'брат мужа','деверь', 'сестра мужа','золовка' ),
/*0b11100*/28 => array( 'родители супругов детей','сваты',     'сват',null,                'сватья',null ),
/*0b11111*/31 => array( 'праправнуки',null,                    'праправнук',null,          'праправнучка',null ),
    
  );
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
    
	public function tableName()
	{
		return 'relation';
	}
	
	public function rules()
	{
		return array(
		  array('path', 'required'),
			array('path', 'in', 'range' => array(Relation::PARENT, Relation::CHILD), 'allowEmpty' => false),
		);
	}
	
	public function relations()
	{
  	return array(
  	  'person1' => array(self::BELONGS_TO, 'Person', 'person1_id'),
  	  'person2' => array(self::BELONGS_TO, 'Person', 'person2_id'),
  	);
	}
  
  public static function name($path /* from person1 to person2 */, $gender2 = null, $gender1 = null, $throwException = true)
  {
    if(!isset(self::$_relations[$path]))
    {
      if($throwException)
        throw new Fam3InternalException('Relation ' . $path . ' (0b' . decbin($path) . ') not defined');
      else
        return false;
    }
    $relation = self::$_relations[$path];
    if(!$gender2)
      return $relation[0];
    else
    {
      if(!$gender1 && isset($relation[6]))
        return $relation[2 + 2*($gender2 == Gender::FEMALE) + 4*($gender1 == Gender::FEMALE)];
      else
        return $relation[2 + 2*($gender2 == Gender::FEMALE)];
    }
  }
  
  public function getName($throwException = true)
  {
    $gender1 = $this->hasRelated('person1') ? $this->getRelated('person1')->getAttribute('gender') : null;
    $gender2 = $this->hasRelated('person2') ? $this->getRelated('person2')->getAttribute('gender') : null;
    return self::name($this->getAttribute('path'), $gender2, $gender1, $throwException);
  }
  
  public function getNameGenetive()
  {
    switch($this->getAttribute('path') . $this->getRelated('person2')->getAttribute('gender'))
    {
      case Relation::PARENT . Gender::MALE:
        return 'отца';
      case Relation::PARENT . Gender::FEMALE:
        return 'мать';
      case Relation::CHILD . Gender::MALE:
        return 'сына';
      case Relation::CHILD . Gender::FEMALE:
        return 'дочь';
    }
  }
  
  public function setPerson1($person)
  {
    $this->setAttribute('person1_id', $person->id);
    $this->person1 = $person;
  }
  
  public function setPerson2($person)
  {
    $this->setAttribute('person2_id', $person->id);
    $this->person2 = $person;
  }
  
  /**
   * Return level of relation (1 for child or parent etc)
   *
   * @param int $path
   * @return int
   */
  public static function level($path)
  {
    return strlen(decbin($path)) - 1;
  }
  
  public static function isFirstLevel($path)
  {
    return $path == Relation::PARENT || $path == Relation::CHILD;
  }
  
  public function getLevel()
  {
    return self::level($this->getAttribute('path'));
  }
  
  public static function getRelations()
  {
    return self::$_relations;
  }
  
  /**
   * Retrieve relation from $person1_id to $person2_id
   *
   * @param Profile|Person|int $person1_id
   * @param Profile|Person|int $person2_id
   * @return Relation
   */
  public static function between($person1_id, $person2_id)
  {
    $p1 = self::_getPersonIdAndGender($person1_id);
    $p2 = self::_getPersonIdAndGender($person2_id);
    
    $index = $p1[0] . '-' . $p2[0];
    
    if(!isset(self::$_relationsBetween[$index]))
    { 
      $relation = Relation::model()->find('person1_id = :person1_id and person2_id = :person2_id', array(
        ':person1_id' => $p1[0], 
        ':person2_id' => $p2[0], 
      ));
      if($relation)
      {
        if($p1[1])
          $relation->person1 = Person::createWithGender($p1[1]);
        if($p2[1])
          $relation->person2 = Person::createWithGender($p2[1]);
      }
    	self::$_relationsBetween[$index] = $relation;
    }
    return self::$_relationsBetween[$index];
  }
  
  /**
   * Return array(person_id, gender)
   *
   * @param Profile|Person|int $profileOrPersonOrPersonId
   * @return array
   */
  private static function _getPersonIdAndGender($profileOrPersonOrPersonId)
  {
    $personId = null;
    $personGender = null;
    if(is_object($profileOrPersonOrPersonId))
    {
      if($profileOrPersonOrPersonId instanceof Profile)
      {
        $personId = $profileOrPersonOrPersonId->getAttribute('person_id');
        $personGender = $profileOrPersonOrPersonId->getGender();
      }
      elseif($profileOrPersonOrPersonId instanceof Person)
      {
        $personId = $profileOrPersonOrPersonId->getAttribute('id');
        $personGender = $profileOrPersonOrPersonId->getAttribute('gender');
      }
      else
        throw new Fam3InternalException('Unexpected class: ' . get_class($profileOrPersonOrPersonId) . '. Only allowed: Profile and Person');
    }
    else
      $personId = $profileOrPersonOrPersonId;
    return array($personId, $personGender);
  }
  
}