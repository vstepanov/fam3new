<?php

class Person extends Model implements IGenderOperated, ITreeOperated
{
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
    
	public function tableName()
	{
		return 'person';
	}
	
	public function rules()
	{
		return array(
			array('gender', 'in', 'range' => array(Gender::MALE, Gender::FEMALE), 'allowEmpty' => true),
		);
	}
	
	public function relations()
	{
  	return array(
  	  'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
  	  'profiles' => array(self::HAS_MANY, 'Profile', 'person_id'),
  	  'creator' => array(self::BELONGS_TO, 'User', 'created_by'),
  	  'user' => array(self::HAS_ONE, 'User', 'person_id'),
  	);
	}
	
	/**
	 * Instance Person with specified gender
	 *
	 * @param string $gender
	 * @return Person
	 */
	public static function createWithGender($gender)
	{
  	$person = new Person;
  	$person->gender = $gender;
  	return $person;
	}
	
	/*public function hasUser()
	{
	  $user = $this->getRelated('user');
	  if($user)
	    return true;
	  else
	    return false;
	}
	
	public function hasAnotherUser()
	{
	  return Yii::app()->user->isGuest || ($user = $this->getRelated('user')) && Yii::app()->user->getId() != $user->getPrimaryKey();
	}*/
	
	// Implementation ITreeOperated begin:
	
	public function hasTheSameTree(/*Person*/ $person)
	{
  	return $this->getAttribute('tree_id') == $person->getAttribute('tree_id');
	}
	
	public function getTreeId()
	{
  	return $this->getAttribute('tree_id');
	}
	
	public function setTreeId($treeId)
	{
  	$this->setAttribute('tree_id', $treeId);
	}
	
	// implementation end
	
	
	// Implementation IGenderOperated begin:
	
	public function getGender()
	{
  	return $this->getAttribute('gender');
	}
	
	public function setGender($gender)
	{
  	$this->setAttribute('gender', $gender);
	}
	
	public function isMale()
  {
    return Gender::isMale($this->getAttribute('gender'));
  }
  
  public function isFemale()
  {
    return Gender::isFemale($this->getAttribute('gender'));
  }
  
  // implementation end
}