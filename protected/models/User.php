<?php

/**
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string $created_at
 * @property integer $is_superuser
 * @property string $lang
 * @property integer $person_id
 */
class User extends Model
{
  private static $_instance = null; // current logged in user
    
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
    
	public function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return array(
			array('email, password', 'required'),
			array('email', 'email'),
			array('email', 'unique'),
			array('email', 'length', 'max' => 45),
		);
	}
	
	public function attributeLabels()
	{
		return array(
		  'email' => 'Email',
		  'password' => 'Пароль',
		);
	}
	
	public function relations()
	{
  	return array(
  	  'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
  	  'setting' => array(self::HAS_ONE, 'Setting', 'user_id'),
  	);
	}
	
	public function hashPassword($password, $salt)
  {
    return md5($salt . $password);
  }
  
  public function setPassword($password)
  {
    $this->salt = $this->_generateSalt();
    $this->password = $this->hashPassword($password, $this->salt);
  }
  
  public function validatePassword($password)
  {
    return $this->hashPassword($password, $this->salt) === $this->password;
  }
  
  /**
   * This function register new User
   * For this need perform 6 actions:
   * 1. create Tree row
   * 2. create Person row and relate to Tree row
   * 3. relate User row to Person row (we do not create User row because already have User row like $this)
   * 4. create Profile row and relate to Person row and User row
   * 5. relate Person row to Profile row
   */
  public function createNewUser()
  {
    $transaction = self::model()->dbConnection->beginTransaction();
    try
    {
      // 1. create Tree row
      $tree = new Tree;
      $tree->save(false);
      
      // 2. create Person row and relate to Tree row
      $person = new Person;
      $person->setAttribute('tree_id', $tree->getAttribute('id'));
      $person->save(false);
      
      // 3. relate User row to Person row
      $this->setPassword($this->password);
      $this->setAttribute('person_id', $person->getAttribute('id'));
      $this->save(false);
      
      // 4. create Profile row and relate to Person row and User row
      $profile = new Profile;
      $profile->setAttribute('user_id', $this->getAttribute('id'));
      $profile->setAttribute('created_by', $this->getAttribute('id'));
      $profile->setAttribute('person_id', $person->getAttribute('id'));
      $profile->save(false);
      
      // 5. relate Person row to Profile row
      $person->setAttribute('profile_id', $profile->getAttribute('id'));
      $person->setAttribute('created_by', $this->getAttribute('id'));
      $person->save(false, array('profile_id'));
      
      $this->person = $person;
      
      $transaction->commit();
      return true;
    }
    catch(Fam3BaseException $e)
    {
      throw $e;
    }
    catch(Exception $e)
    {
      $transaction->rollback();
      throw new Fam3InternalException($e);
      return false;
    }
  } 
  
  /**
   * Retrieve current User object if user logged in
   *
   * @return User
   */
  public static function instance()
  {
    if(is_null(self::$_instance))
    {
      if(Yii::app()->user->isGuest)
      {
        self::$_instance = new User;
        self::$_instance->person = new Person;
        self::$_instance->person->profile = new Profile;
      }
      else
        self::$_instance = self::retrieve(Yii::app()->user->id);
      self::$_instance->person->profile->person = self::$_instance->person;
    }
    return self::$_instance;
  }
  
  public static function retrieve($userId)
  {
    $user = User::model()->with('person')->with('person.profile')->findByPk($userId);
    $user->getRelated('person')->getRelated('profile')->person = $user->getRelated('person');
    return $user;
  }
  
  /**
   * Similar to Profile::url
   */
  public function getLink()
  {
    return Profile::url($this->getRelated('person')->getAttribute('profile_id'));
  }
  
  /**
   * @return Profile
   */
  public function getProfile()
  {
    return $this->getRelated('person')->getRelated('profile');
  }
  
  public function getPerson()
  {
    return $this->getRelated('person');
  }
  
  protected function _generateSalt()
  {
    return uniqid('', true);
  }
}