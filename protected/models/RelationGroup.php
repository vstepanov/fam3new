<?php

class RelationGroup extends CComponent
{
  private $_relations = null;
  
  public function __construct($relations = null)
  {
    $this->_relations = is_array($relations) ? $relations : array();
  }
  
  /**
   * @param int || Profile || Person $person1
   * @param int || Profile || Person $person2
   * @param int $path
   * @return RelationGroup
   */
  public function add($person1, $person2, $path)
  {
    $this->_relations[] = array($this->_getPersonId($person1), $this->_getPersonId($person2), $path);
  }
  
  public function addRelation(Relation $relation)
  {
    $this->add($relation->getAttribute('person1_id'), $relation->getAttribute('person2_id'), $relation->getAttribute('path'));
  }
  
  /**
   * Insert relations and return number of inserted
   *
   * @return int
   */
  public function save()
  {
    if(count($this->_relations) == 0)
      return;
    $this->_addBackRelations();
    $values = '';
    foreach($this->_relations AS $rel)
      $values .= ($values ? ',' : '') . '(' . $rel[0] . ', ' . $rel[1] . ', ' . $rel[2] . ')';
    $sql = 'replace into ' . Relation::tableName() . ' (person1_id, person2_id, path) values ' . $values;
    $command = Relation::model()->dbConnection->createCommand($sql);
    return $command->execute();
  }
  
  /**
   * Mark relations between $person1 and any, between $person2 and any, with path level > 1 as deprecated
   *
   * @return int
   */
  public function markAsDeprecated()
  {
    $persons = array();
    foreach($this->_relations AS $rel)
    {
      if(!in_array($rel[0], $persons))
        $persons[] = $rel[0];
      if(!in_array($rel[1], $persons))
        $persons[] = $rel[1];
    }
    if(count($persons) == 0)
      return 0;
    $sql = 'update ' . Relation::tableName() . ' set deprecated = 1 where (person1_id in ('.implode(',', $persons).') or person2_id in ('.implode(',', $persons).')) and length(bin(path)) > 2 ';
    $conection = Relation::model()->dbConnection;
    $command = Relation::model()->dbConnection->createCommand($sql);
    return $command->execute();
  }
  
  /**
   * @param int || Tree $treeId
   */
  public function loadAllRelationsByTree($treeId)
  {
    if(is_object($treeId) && $treeId instanceof Tree)
      $treeId = $treeId->getAttribute('id');
    $sql = '
select
	r.person1_id, r.person2_id, path
from ' . Relation::tableName() . ' r
left join person p on (r.person1_id = p.id or r.person2_id = p.id)
where p.tree_id = :tree_id
group by r.person1_id, r.person2_id';
    $command = Relation::model()->dbConnection->createCommand($sql);
  	$command->bindParam(":tree_id", $treeId, PDO::PARAM_INT);
  	$this->_relations = $command->queryAll(false);
  }
  
  /**
   * Search back relations for 1-level relations and add to $this->_relations
   */
  private function _addBackRelations()
  {
    $needBack = array();
    foreach($this->_relations AS $rel)
    {
      $path = $rel[2];
      if(Relation::isFirstLevel($path))
      {
        if(isset($needBack[$rel[1]]))
          unset($needBack[$rel[1]]);
        else
          $needBack[$rel[0]] = array($rel[1], $path);
      }
    }
    foreach($needBack AS $person => $rel)
      $this->_relations[] = array($rel[0], $person, $rel[1] == Relation::PARENT ? Relation::CHILD : Relation::PARENT);
  }
  
  /**
   * @param int || Profile || Person $person
   * @return int
   */
  private function _getPersonId($person)
  {
    if(is_object($person))
    {
      if($person instanceof Profile)
        $id = $person->getAttribute('person_id');
      elseif($person instanceof Person)
       $id = $person->getAttribute('id');
      else
        throw new Fam3InternalException('Unsupported object type: ' . get_class($person));
    }
    else
      $id = $person;
    if(!$id)
      throw new Fam3InternalException('Empty id');
    return $id;
  }
}