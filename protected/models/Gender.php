<?php

class Gender extends CComponent
{
  const MALE    = 'm';
  const FEMALE  = 'f';
  
  public static function getName($gender)
  {
    switch($gender)
    {
      case Gender::MALE:    return 'Мужской';
      case Gender::FEMALE:  return 'Женский';
      default:              throw new Fam3InternalException('Trying to get name of undefined gender');
    }
  }
  
  public static function getList()
  {
    return array(Gender::MALE, Gender::FEMALE);
  }
  
  public static function isValid($gender)
  {
    return $gender == Gender::MALE || $gender == Gender::FEMALE;
  }
  
  public static function validate($gender)
  {
    if(!Gender::isValid($gender))
      throw new Fam3InternalException('Gender is not valid');
  }
  
  public static function invert($gender)
  {
    self::validate($gender);
    return $gender == Gender::MALE ? Gender::FEMALE : Gender::MALE;
  }
  
  public static function isMale($gender)
  {
    return $gender == Gender::MALE;
  }
  
  public static function isFemale($gender)
  {
    return $gender == Gender::FEMALE;
  }
}