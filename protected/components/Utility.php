<?php

class Utility extends CComponent
{
  public static function mb_ucfirst($word)
  { 
    return mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr(mb_convert_case($word, MB_CASE_LOWER, 'UTF-8'), 1, mb_strlen($word), 'UTF-8');
  }
  
  public static function mb_str_pad($input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
  {
    $diff = strlen($input) - mb_strlen($input);
    return str_pad($input, $pad_length+$diff, $pad_string, $pad_type);
  }
  
  public static function nl2br_limit($string, $num)
  { 
    return preg_replace('/\n/', '<br/>', preg_replace('/(\s{'.$num.'})\s+/','$1', $string));
  }
}