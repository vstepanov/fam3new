<?php

class Fam3AccessDeniedException extends Fam3BaseException
{
  public function __construct($message = 'Доступ запрещен')
  {
    if(Yii::app()->user->isGuest)
      Yii::app()->user->loginRequired();
    else
      parent::__construct($message, 403);
  }
}