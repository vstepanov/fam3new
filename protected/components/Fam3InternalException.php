<?php

/**
 * Ошибки в логике, нарушающие работу, их необходимо логгировать
 */

class Fam3InternalException extends Fam3BaseException
{
  public function __construct($message)
  {
    parent::__construct($message, 500);
  }
}