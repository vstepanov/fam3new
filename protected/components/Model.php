<?php

class Model extends CActiveRecord
{
  protected $_changes; // array(attr => array(old, new)) of changed attributes
  private $_lookForChanges = false; // enabled by ::lookForChanges(), disabled by ::getChanges()
  
  public function lookForChanges()
  {
    $this->_lookForChanges = true;
    $this->_changes = array();
  }
  
  public function getChanges()
  {
    return $this->_changes;
  }
  
  public function wasChanged()
  {
    return count($this->_changes);
  }
  
  public function setAttributes($values, $safeOnly = true)
  {
    if($this->_lookForChanges)
      $oldAttributes = $this->getAttributes();
    parent::setAttributes($values, $safeOnly);
    if($this->_lookForChanges)
    {
      $this->_searchChanges($oldAttributes);
      $this->_lookForChanges = false;
    }
  }
  
  protected function _searchChanges($oldAttributes)
  {
    foreach($this->getAttributes() as $k => $v)
    {
      if((!isset($oldAttributes[$k]) && $v) || (isset($oldAttributes[$k]) && $oldAttributes[$k] != $v))
        $this->_changes[$k] = array($oldAttributes[$k], $v);
    }
  }
}