<?php
 
class WebUser extends CWebUser
{
  public function loginAsUser(User $user)
  {
    $identity = new DummyUserIdentity($user->id, $user->email);
    $this->login($identity);
  }
  
  public function login($identity, $duration = null)
  {
    if(is_null($duration)) $duration = 3600*24*30;
    return parent::login($identity, $duration);
  }
  
  public function getProfileId()
  {
    if($this->isGuest)
      throw new Fam3InternalException('Guest can not access his profile id');
    else if(!$this->hasState('profile_id'))
      $this->setState('profile_id', User::instance()->getRelated('person')->getAttribute('profile_id'));
    return $this->getState('profile_id');
  }
  
  public function getPersonId()
  {
    if($this->isGuest)
      throw new Fam3InternalException('Guest can not access his person id');
    else if(!$this->hasState('person_id'))
      $this->setState('person_id', User::instance()->getAttribute('person_id'));
    return $this->getState('person_id');
  }
  
  public function getLink()
  {
    return Profile::url($this->getProfileId());
  }
}