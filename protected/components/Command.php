<?php
class Command extends CConsoleCommand
{
  private $_opts = array();
  
  protected function addOpt($opt, $val)
  {
    if(!is_null($val))
      $this->_opts[$opt] = 1;
  }
  
  protected function opt($opt)
  {
    return isset($this->_opts[$opt]);
  }
  
  protected function writeln($text = '')
  {
    echo $text . "\r\n";
  }
  
  protected function write($text = '')
  {
    echo $text . " ";
  }
}