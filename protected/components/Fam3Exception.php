<?php

/**
* Ошибки, которые должны быть перехвачены в контроллере или модели
*/

class Fam3Exception extends Fam3BaseException
{
  public function __construct($message)
  {
    parent::__construct($message, 400);
  }
}