<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/mainwrapped';
  public $page;               // текущая страница
  public $profile;            // текущий профиль
  
  public function pageIs($page)
  {
    return $this->page == $page;
  }
  
  protected function _initProfile($profileId)
  {
    if(!Yii::app()->user->isGuest && $profileId == Yii::app()->user->getProfileId())
      return $this->profile = Profile::instance(User::instance()->getProfile());
    else
      return $this->profile = Profile::instance($profileId);
  }
  
  protected function checkAuth()
  {
    if(Yii::app()->user->isGuest)
      Yii::app()->user->loginRequired();
  }
  
  protected function _saveFeed($recipient, $feedAttributes)
  {
    $feed = new Feed('insert');
    $feed->attributes = $feedAttributes;
    $feed->setAttribute('author_id', Yii::app()->user->getProfileId());
    $feed->setRecipient($recipient);
    if($feed->save())
      $this->redirect(Yii::app()->request->requestUri);
  }
}