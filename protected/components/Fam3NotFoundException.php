<?php

class Fam3NotFoundException extends Fam3BaseException
{
  public function __construct($message)
  {
    parent::__construct($message, 404);
  }
}