<?php

class Fam3LoginRequiredException extends Fam3BaseException
{
  public function __construct ()
  {
    Yii::app()->user->loginRequired();
  }
}