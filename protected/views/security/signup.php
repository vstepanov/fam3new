<h1>Регистрация</h1>

<div>

<?php $form = $this->beginWidget('CActiveForm', array(
  'id' => 'signup-form',
  'action' => $this->createUrl('security/signup'),
  'enableAjaxValidation' => false,
  'enableClientValidation' => false,
  'htmlOptions' => array('class' => 'forms'),
  'errorMessageCssClass' => 'error',
)); ?>

  <ul>
    <li>
      <?php echo $form->error($model, 'email'); ?>
      <?php echo $form->emailField($model, 'email', array('placeholder' => $model->getAttributeLabel('email'))); ?>
    </li>
    <li>
      <?php echo $form->error($model, 'password'); ?>
      <?php echo $form->passwordField($model, 'password', array('placeholder' => $model->getAttributeLabel('password'))); ?>
    </li>
    <li>
      <input type="submit" class="btn" value="Зарегистрироваться" />
    </li>
    <li class="links">
      <a href="<?php echo $this->createUrl('security/signin')?>">Уже зарегистрированы?</a>
    </li> 	        		
  </ul>

<?php $this->endWidget(); ?>

</div>