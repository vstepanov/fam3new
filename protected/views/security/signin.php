<h1>Вход</h1>

<div>

<?php $form = $this->beginWidget('CActiveForm', array(
  'id' => 'signin-form',
  'action' => $this->createUrl('security/signin'),
  'enableAjaxValidation' => false,
  'enableClientValidation' => false,
  'htmlOptions' => array('class' => 'forms'),
  'errorMessageCssClass' => 'error',
)); ?>

  <ul>
    <li>
      <?php echo $form->error($model, 'email'); ?>
      <?php echo $form->emailField($model, 'email', array('placeholder' => $model->getAttributeLabel('email'))); ?>
    </li>
    <li>
      <?php echo $form->error($model, 'password'); ?>
      <?php echo $form->passwordField($model, 'password', array('placeholder' => $model->getAttributeLabel('password'))); ?>
    </li>
    <li>
      <input type="submit" class="btn" value="Войти" />
    </li>
    <li class="links">
      <a href="<?php echo $this->createUrl('security/signup')?>">Еще не зарегистрированы?</a>
    </li> 
  </ul>

<?php $this->endWidget(); ?>

</div>