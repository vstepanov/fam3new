<?php $this->beginContent('//layouts/main'); ?>

  <?php $this->widget('application.widgets.ProfileWidget', array('profile' => $this->profile)) ?>

  <div class="relatives_and_content_container row container split clear">
  
    <div class="third relatives_container">
      <?php $this->widget('application.widgets.RelativesWidget', array('profile' => User::instance()->getProfile())) ?>
    </div><!--/third-->
    
    <div class="twothird">
      <div class="content_container">
        <div id="content">
          <?php echo $content?>
    	  </div><!--/content-->
      </div><!-- /content_container -->
  	</div><!--/twothird-->
  	
  </div><!--/relatives_and_content_container-->
	
<?php $this->endContent(); ?>