<!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/glyphicons.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/halflings.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/kube.css" /> 	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/base.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/profile.css" /> 	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/feed.css" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/settings.css" /> 
	
  <!--[if lt IE 9]>
  <script>
  	var head = document.getElementsByTagName('head')[0], style = document.createElement('style');
  	style.type = 'text/css';
  	style.styleSheet.cssText = ':before,:after{content:none !important';
  	head.appendChild(style);
  	setTimeout(function(){ head.removeChild(style); }, 0);
  </script>
  <![endif]-->		

	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-hashchange.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fam3.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fam3.urldispatcher.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fam3.feed.js"></script>
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>	

</head>
<body>
  <div id="container">
  
    <?php echo $content ?>
    
	  <footer id="footer">
      <div class="wrapper">
        <div class="subwrapper">
          <ul>
            <li><a href="/about">О сайте</a></li>
            <li><a href="/help">Помощь</a></li>
            <li><a href="">Условия</a></li>
            <li>fam3 &copy; 2013</li>
            <li><?php echo sprintf('%.2f', Yii::getLogger()->getExecutionTime()*1000)?>ms</li>
            <li><?php echo sprintf('%.2f', memory_get_peak_usage()/1048576)?>mb</li>
          </ul>
        </div><!-- /subwrapper -->
      </div><!--/wrapper-->
    </footer><!--/footer-->
        
  </div><!--/container-->
</body>
</html>