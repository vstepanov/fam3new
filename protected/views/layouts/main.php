<?php $this->beginContent('//layouts/_base'); ?>

  <header id="header" class="group">
    <div class="wrapper">
      <div class="subwrapper">
        <h1 class="fam3"><a href="/">fam3<sup>beta</sup></a></h1>
        <?php echo $this->renderPartial('//layouts/_topmenu', null, true)?>
      </div><!-- /subwrapper -->
    </div><!--/wrapper-->
  </header><!--/header-->

	<div id="main">
		<div class="wrapper">	
			<?php echo $content?>
		</div><!--/wrapper-->
	</div><!--/main-->
	
<?php $this->endContent(); ?>