<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/security.css'); ?>

<?php $this->beginContent('//layouts/main'); ?>
<div class="subwrapper">
  <?php echo $content ?>
</div><!-- /subwrapper -->
<?php $this->endContent(); ?>