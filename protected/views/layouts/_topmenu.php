<?php
  if(Yii::app()->user->isGuest)
    $menu = array('signin' => 'Вход', 'signup' => 'Регистрация');
  else
    $menu = array('mypage' => 'Моя страница', 'messages' => 'Сообщения', 'feed' => 'Общая лента', 'settings' => 'Настройки');
?>

<?php if(isset($menu)):?>
<nav id="nav">
	<ul>
	  <?php foreach($menu AS $page => $name):?>
	    <li>
	      <?php if($this->pageIs($page)):?>
	        <span><?php echo $name ?></span>
	      <?php else:?>
	        <?php echo CHtml::link($name, $page == 'mypage' ? Profile::url(Yii::app()->user->getProfileId()) : '/' . $page) ?>
	      <?php endif ?>
	    </li>
	  <?php endforeach ?>									
	</ul>				
</nav>
<?php endif ?>