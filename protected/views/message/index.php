<div class="feed_container without_tabs ?>">

  <div class="relatives_and_feed_container row split">
    <div class="third relatives_container">
      <div class="relatives subwrapper"><div class="relation_group">
        <ul><li<?php if(!Profile::hasInstance()):?> class="current"<?php endif ?>><a href="<?php echo Yii::app()->createUrl('message/index')?>" class="profile">Все сообщения</a></li></ul>
      </div></div>
      <?php $this->widget('application.widgets.RelativesWidget', array(
        'profile' => User::instance()->getProfile(),
        'showMessageEmpty' => $feedReader->getCount(),
        'showGroupCountProfiles' => false,
        'linkHandler' => function($p) {return $p->customLink(Yii::app()->createUrl('message/profile', array('profileId' => $p->getPrimaryKey())));},
        'relatives' => $contractorsByPath
      )) ?>
    </div>
    <div class="twothird">
      
      <hgroup class="subwrapper">
        <h1>Сообщения</h1>
        <h2 class="subheader">здесь отображается личная переписка</h2>
      </hgroup>
  
      <?php $this->widget('application.widgets.FeedWidget', array('feedReader' => $feedReader)) ?>
      
    </div><!-- /twothird -->
  </div><!-- /row -->

</div><!-- /feed_container -->