<div class="subwrapper">

<h3>Добавить <?php echo $relation->getNameGenetive()?><?php /* для <?php echo $this->profile->link()?>*/?></h3>

<hr>

<?php $form = $this->beginWidget('CActiveForm', array(
  'id' => 'profile-edit-form',
  'enableAjaxValidation' => false,
  'enableClientValidation' => false,
  'htmlOptions' => array('class' => 'forms columnar'),
  'errorMessageCssClass' => 'error',
)); ?>

  <ul>
    <li>
      <?php echo $form->label($profile,'name'); ?>
      <?php echo $form->textField($profile, 'name'); ?>
    </li>
    <li>
      <?php echo $form->label($profile,'middlename'); ?>
      <?php echo $form->textField($profile, 'middlename'); ?>
    </li>
    <li>
      <?php echo $form->label($profile,'surname'); ?>
      <?php echo $form->textField($profile, 'surname'); ?>
    </li>
    <li>
			<?php echo $form->label($profile,'birthd'); ?>
      <?php echo $form->textField($profile, 'birthd', array('size' => 10, 'maxlength' => 10)); ?>
      <span class="descr">дд.мм.гггг</span>
      <?php echo $form->error($profile, 'birthd'); ?>
		</li>
		<li>
      <?php echo $form->label($profile,'birth_country'); ?>
      <?php echo $form->textField($profile, 'birth_country'); ?>
    </li>
    <li>
      <?php echo $form->label($profile,'birth_place'); ?>
      <?php echo $form->textField($profile, 'birth_place'); ?>
      <span class="descr">Область, город</span>
    </li>
    <li class="push">
      <input type="submit" class="btn" value="Сохранить" />
      <a href="<?php echo $this->profile->getLink()?>" class="btn">Отменить</a>
    </li>
  </ul>

<?php $this->endWidget(); ?>

</div>