<?php
  $tabs = array('feed' => 'Лента', 'relatives' => 'Родственники', 'photo' => 'Фото');
  if(!$this->profile->isMy() && $this->profile->hasUser())
    $tabs['private'] = 'Переписка';
?>

<nav class="tabs_container nav-tabs subwrapper">
  <ul>
    <?php foreach($tabs AS $tab => $name):?>
      <li><a href="<?php echo $this->profile->getLink($tab)?>"<?php if($tab == $this->tab):?> class="active"<?php endif ?>><?php echo $name?></a></li>
    <?php endforeach ?>
  </ul>
</nav><!-- /tabs_container nav-tabs subwrapper -->