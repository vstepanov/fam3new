<div class="subwrapper">

  <h3>
    <?php if(isset($title)): echo $title ?>
    <?php else: ?>
      Укажите пол
    <?php endif ?>
  </h3>
  
  <hr>
  
  <form action="<?php echo $this->createUrl('profile/setGender', array('profileId' => $this->profile->getPrimaryKey()))?>" method="POST">
    <?php foreach(Gender::getList() AS $gender):?>
      <input type="submit" name="Gender[<?php echo $gender?>]" class="btn" value="<?php echo Gender::getName($gender)?>">
    <?php endforeach ?>
    <?php if(isset($nextAction)):?>
      <input type="hidden" name="nextAction" value="<?php echo $nextAction?>">
    <?php endif ?>
  </form>
  
</div>