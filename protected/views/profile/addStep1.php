<div class="subwrapper">

<h3>Добавить родственника<?php /* для <?php echo $this->profile->link()?>*/?></h3>

<hr>

  <?php if(!$hasFather):?>
    <a href="<?php echo $this->profile->getLink('add', array('relationName' => 'father')) ?>" class="btn">отец</a>
  <?php endif ?>
  
  <?php if(!$hasMother):?>
    <a href="<?php echo $this->profile->getLink('add', array('relationName' => 'mother')) ?>" class="btn">мать</a>
  <?php endif ?>
  
  <a href="<?php echo $this->profile->getLink('add', array('relationName' => 'son')) ?>" class="btn">сын</a>
  
  <a href="<?php echo $this->profile->getLink('add', array('relationName' => 'daughter')) ?>" class="btn">дочь</a>
  
  <a href="<?php echo $this->profile->getLink()?>" class="btn">Отменить</a>

</div>