<?php
  $canWrite = !$feedReader->typeIs(FeedReader::TYPE_SELF) && !Yii::app()->user->isGuest && !($feedReader->typeIs(FeedReader::TYPE_PRIVATE) && $this->profile->isMy());
?>

<div class="feed_container <?php if($canWrite) echo 'can_write'?>">
  
  <?php if(Yii::app()->user->isGuest || !$this->profile->getRelated('person')->hasTheSameTree(User::instance()->getRelated('person'))):?>
    
    <div class="subwrapper">Вы не можете читать эту ленту</div>
    
  <?php else: ?>
    
    <?php echo $this->renderPartial('_tabs', null, true) ?>
    
    <?php $this->widget('application.widgets.FeedWidget', array('feedReader' => $feedReader)) ?>
    
  <?php endif ?>

</div><!-- /feed_container -->