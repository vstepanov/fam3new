<div class="subwrapper">

  <h3>Указание родственных связей</h3>
  
  <hr>

  <form method="post" class="forms">
    <ul>
      <li>
        Вы можете указать
        <?php echo $path == Relation::PARENT ? 'детей' : (Gender::invert($this->profile->getGender()) == Gender::MALE ? 'отца' : 'мать') ?>
        для <?php echo $relatedProfile->link(array('target' => '_blank'))?>:
      </li>
      <?php foreach($profiles AS $index => $profile):?>
        <li>
          <label><input type="<?php echo $path == Relation::PARENT ? 'checkbox' : 'radio'?>" name="Relate[person]<?php echo $path == Relation::PARENT ? '[]' : ''?>" value="<?php echo $profile->getAttribute('person_id')?>" <?php if($index == 0 && $path == Relation::CHILD):?>checked="checked"<?php endif ?> /> <?php echo $profile->arlink(array('target' => '_blank'))?></label>
        </li>
      <?php endforeach ?>
      <li>
        <input type="submit" class="btn" name="Relate[save]" value="Сохранить">
        <input type="submit" class="btn" name="Relate[skip]" value="Пропустить">
      </li>
    </ul>
  </form>


</div>