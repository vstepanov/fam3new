<?php $profile = $feed->getRelated('author')?>
Отредактировал<?php if($profile->isFemale()) echo 'а'?> профиль:
<ul>
<?php foreach($changes AS $k => $v):?>
  <li>
    <strong><?php echo $profile->getAttributeLabel($k)?>:</strong>
    <?php if($v[0]):?>
      <del><?php echo $v[0]?></del>
    <?php endif ?>
    <?php if($v[1]) echo $v[1]?>
  </li>
<?php endforeach ?>
</ul>