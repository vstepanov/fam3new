<div id="main">
  <div id="hheader">
    <div class="wrapper"><div class="subwrapper"><div class="inner">
      <h1 class="fam3">fam3<sup>beta</sup></h1>
      <h1 class="subheader">Поможет быть на связи с родственниками</h1>
      <br>
      <div id="showcase" class="row container">
        <div class="half promo"><img src="/img/screen2.png"></div>
        <div class="half">
          <ul class="features">
            <li>простой интерфейс</li>
            <li>удобный список родственников</li>
            <li>семейная лента сообщений</li>
            <li>совместное составление древа</li>
            <li>поиск дальних родственников</li>
            <li>полностью бесплатно</li>
          </ul>
        </div>
      </div>
    </div></div></div><!-- /wrappers -->
  </div><!-- /hmain -->
  
  <?php /*<div id="showcase" style="background-color: #cc0033;padding:1em 0;">
    <div class="wrapper"><div class="subwrapper">
      <p style="background-color: #fff;border: #cc0033 solid 0px; box-shadow: 0 2px 5px #333;padding: 1.5em 1em;"><img src="/img/screen5.png"></p>
    </div></div>
  </div>*/ ?>
  
  <br>
  
  <div class="wrapper"><div class="subwrapper">
    <div class="row">
      <div class="twofifth">
        <div class="join"><div class="joininner">
          <hgroup>
            <h2>Присоединяйтесь</h2>
            <h2 class="subheader">введите e-mail и пароль:</h2>
          </hgroup>
          <form class="forms mainform" method="post"  action="/signin">
            <ul>
              <li><input type="text" name="email" placeholder="Email" size="25"></li>
              <li><input type="password" name="password" placeholder="Пароль" size="25"></li>
              <li>
                <input type="submit" value="Вход" class="btn submit signin">
                <input type="submit" value="Регистрация" class="submit mainbutton signup">
              </li>
            </ul>
          </form>
          <script type="text/javascript">
            $('.mainform .signup').click(function() {
              $('.mainform').attr('action', '/signup');
            });
          </script>
        </div></div>
        <div class="joininner">
          <h2 class="subheader clear">или войдите через:</h2>
          <ul class="social">
            <li><a href="#" title="ВКонтакте"><img src="/img/ico/vkontakte.png"></a></li>
            <li><a href="#" title="Facebook"><img src="/img/ico/facebook.png"></a></li>
            <li><a href="#" title="Twitter"><img src="/img/ico/twitter.png"></a></li>
            <li><a href="#" title="Main.RU"><img src="/img/ico/mail-ru.png"></a></li>
            <li><a href="#" title="Google+"><img src="/img/ico/google-plus.png"></a></li>
          </ul>
        </div>
      </div>
      <div class="threefifth thesis">
        <h2><i class="glyphicons-icon tree_deciduous"></i>Семейное древо</h2>
        <p>Создание семейного древа очень увлекательное занятие. Fam3 поможет сгруппировать и упорядочить список Ваших родственников в удобном виде.</p>
        <h2><i class="glyphicons-icon conversation"></i>Общение в кругу семьи</h2>
        <p>В жизни мы привыкли разделять общение с родственниками и друзьями. Fam3 создан для общения с родственниками.</p>
        <h2><i class="glyphicons-icon parents"></i>Поиск родственников</h2>
        <p>Как правило, мы плохо знаем родственников дальше троюродных братьев и сестер. Fam3 поможет найти родство в древах других пользователей.</p>
      </div>
    </div>    
  </div></div><!-- /wrappers -->
</div><!-- /main -->