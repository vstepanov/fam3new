<?php

return
  array(
    'base_url' => 'http://'. $_SERVER['SERVER_NAME'] . '/authEndpoint', 

		"providers" => array ( 
			// openid providers
			"OpenID" => array (
				"enabled" => true
			),

			"Yahoo" => array ( 
				"enabled" => true,
			),

			"AOL"  => array ( 
				"enabled" => true 
			),

			"Google" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "764592195231.apps.googleusercontent.com", "secret" => "O-vpDUCU-cMFYgWHHKmyUNit" ), 
			),

			"Facebook" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" ), 
			),

			"Twitter" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "j34hYtsltLpLZQAOGxZmA", "secret" => "0pQeyLZE5xRl6Wl4BA2imuSRxqlAdJA9Zmq3O7U" ) 
			),

			// windows live
			"Live" => array ( 
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),

			"MySpace" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" ) 
			),

			"LinkedIn" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" ) 
			),

			"Foursquare" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" ) 
			),
		),
    
    "debug_mode" => true,
    
    "debug_file" => dirname(__FILE__).'/../runtime/hybridauth.log',
  );