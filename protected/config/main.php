<?php

setlocale(LC_ALL, 'ru_RU','rus_RUS','Russian');

date_default_timezone_set('Europe/Moscow');

mb_regex_encoding('UTF-8');
mb_internal_encoding('UTF-8');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'fam3',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	
	// application components
	'components'=>array(
		'user'=>array(
		  'class' => 'WebUser',
      'allowAutoLogin'=>true,
      'loginUrl' => array('/signin'),
		),
		
		'urlManager'=>array(
		  'showScriptName' => false,
			'urlFormat'=>'path',
			'rules'=> require_once(dirname(__FILE__) . '/routing.php'),
		),
		
		/*'cache'=>array(
      'class'=>'CDbCache',
    ),*/ // временно убрал т.к. вызывало ошибку при развертывании на продуктивный сервер
		
		'db' => require_once(dirname(__FILE__) . '/db.php'),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
      'errorAction'=>'default/error',
    ),
    
		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
			  array(
          'class'=>'CProfileLogRoute',
          'report'=>'summary',
          // Показывает время выполнения каждого отмеченного блока кода.
          // Значение "report" также можно указать как "callstack".
        ),
				/*array(
					'class'=>'CWebLogRoute',
					'categories'=>'system.db.*',
				),
			),
		),*/
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);