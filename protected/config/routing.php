<?php

return array(
  '/' => 'default/index',
  'signin' => 'security/signin',
  'signup' => 'security/signup',
  'signout' => 'security/signout',
  
  'feed' => 'feed/index',
  'feed/p<profileId:\d+>' => 'feed/profile',
  
  'messages' => 'message/index',
  'messages/p<profileId:\d+>' => 'message/profile',
  
  'p<profileId:\d+>' => 'profile/index',
  'p<profileId:\d+>/edit' => 'profile/edit',
  'p<profileId:\d+>/add/<relationName:\w+>' => 'profile/add',
  'p<profileId:\d+>/add' => 'profile/add',
  'p<profileId:\d+>/setGender' => 'profile/setGender',
  'p<profileId:\d+>/relate/p<relatedProfileId:\d+>' => 'profile/relate',
  #'p<profileId:\d+>/feed/<type:\w+>' => 'profile/feed',
  'p<profileId:\d+>/private' => 'profile/private',
  'p<profileId:\d+>/relatives' => 'profile/relatives',
  'p<profileId:\d+>/photo' => 'profile/photo',
  
  'settings' => 'setting/index',
  
  '<controller:\w+>/<id:\d+>'=>'<controller>/view',
  '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
  '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);