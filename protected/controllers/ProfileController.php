<?php

class ProfileController extends Controller
{ 
  public $layout = 'profile';
  public $tab;

  public function actionIndex($profileId)
  {
    $this->actionFeed($profileId);
  }
  
  public function actionFeed($profileId, $type = null)
  {
    $profile = $this->_initProfile($profileId);
    if(isset($_POST['Post']))
      $this->_saveFeed($profile, $_POST['Post']);
    if(is_null($type))
    {
      $type = FeedReader::TYPE_PUBLIC;
      $this->tab = 'feed';
    }
    if($profile->isMy())
      $this->page = "mypage";
    try
    {
      $feedReader = new FeedReader($profile, $type);
      $feedReader->load();
      $this->render('feed', array('feedReader' => $feedReader));
    }
    catch(Fam3AccessDeniedException $e)
    {
      $this->render('warning', array('exception' => $e));
    }
  }
  
  public function actionEdit($profileId)
  {
    $this->_initProfile($profileId);
    
    if(!$this->profile->hasTheSameTree(User::instance()->getProfile()) || $this->profile->hasAnotherUser())
      throw new Fam3AccessDeniedException;
      
    $this->page = "profile-edit";
    
    $profile = $this->profile;
    $profile->setScenario('update');
    $profile->person->setScenario('update');
    if(isset($_POST['Profile']))
		{
		  $profile->lookForChanges();
			$profile->attributes = $_POST['Profile'];
			if(isset($_POST['Person']))
			  $profile->person->attributes = $_POST['Person'];
			if($profile->validate() && $profile->person->validate())
			{
			  $transaction = Profile::model()->dbConnection->beginTransaction();
			  try
			  {
  			  $profile->save(false);
  			  if(isset($_POST['Person']) && !$profile->hasChilds())
  			    $profile->person->save(false, array('gender'));
  			  if($profile->wasChanged())
  			    Feed::createSystemProfileEdit($profile->getChanges(), $profile->getPrimaryKey());
  			  $transaction->commit();
  			  $this->redirect($profile->getLink());
			  }
			  catch(Fam3BaseException $e)
			  {
  			  throw $e;
			  }
			  catch(Exception $e)
			  {
  			  $transaction->rollback();
          throw new Fam3InternalException($e);
			  }
			}
    }
    $this->render('edit', array('profile' => $profile));
  }
  
  public function actionRelatives($profileId)
  {
    $this->tab = 'relatives';
    $profile = $this->_initProfile($profileId);
    if($profile->isMy())
      $this->page = "mypage";
    $this->render('relatives');
  }
  
  public function actionPhoto($profileId)
  {
    $this->tab = 'photo';
    $profile = $this->_initProfile($profileId);
    if($profile->isMy())
      $this->page = "mypage";
    $this->render('photo');
  }
  
  public function actionPrivate($profileId)
  {
    $this->tab = 'private';
    $this->actionFeed($profileId, FeedReader::TYPE_PRIVATE);
  }
  
  public function actionRelate($profileId, $relatedProfileId)
  {
    try
    {
      $profile = $this->_initProfile($profileId);
      $this->_validateGender();
      $profiles = array();
      $personsIds = array();
      $relatedProfile = Profile::retrieve($relatedProfileId);
      if(!$relatedProfile)
        throw new Fam3NotFoundException('Связываемый профиль не найден');
      $profile->loadRelatives(array(Relation::PARENT, Relation::CHILD));
      $relatedProfile->loadRelatives(array(Relation::PARENT, Relation::CHILD));
      $path = $profile->getPath($relatedProfile);
      if(!$path)
        throw new Fam3NotFoundException('Не найдена прямая связь между профилями');
        
      // если я добавил отца, то надо взять всех моих братьев не имеющих отца и предложить их связать с отцом
      if($path == Relation::PARENT)
      {
        $secondParent = $profile->getParent(Gender::invert($relatedProfile->getGender()));
        if($secondParent)
        {
          $profileSiblings = $secondParent->getChilds();
          $relatedProfileChilds = $relatedProfile->getChilds();
          foreach($profileSiblings AS $child)
          {
            if($child->getPrimaryKey() == $profile->getPrimaryKey() || isset($relatedProfileChilds[$child->getPrimaryKey()]) || $child->hasParent($relatedProfile->getGender()))
              continue;
            $profiles[] = $child;
            $personsIds[] = $child->getAttribute('person_id');
          }
        }
      }
      // если я добавил ребенка, то надо предложить выбрать мать этого ребенка из всех матерей моих детей
      elseif($path == Relation::CHILD)
      {
        if($relatedProfile->hasBothParents())
          $this->redirect($relatedProfile->getLink());
        $secondParentGender = Gender::invert($profile->getGender());
        foreach($profile->getChilds() AS $child)
        {
          if($child->getPrimaryKey() == $relatedProfile->getPrimaryKey())
            continue;
          $parent = $child->getParent($secondParentGender);
          if($parent && !isset($profiles[$parent->getPrimaryKey()]))
          {
            $profiles[$parent->getPrimaryKey()] = $parent;
            $personsIds[] = $parent->getAttribute('person_id');
          }
        }
      }
      else
        throw new Fam3InternalException('Unexpected relation: ' . $path . '. Only allowed: ' . Relation::PARENT . ' and ' . Relation::CHILD);
      if(count($profiles) == 0)
        $this->redirect($relatedProfile->getLink());
        
      if(isset($_POST['Relate']))
      {
        if(isset($_POST['Relate']['save']))
        {
          $_personsIds = $_POST['Relate']['person'];
          // вылидация
          if($path == Relation::CHILD)
          {
            if(is_array($_personsIds))
              throw new Fam3InternalException('Expect only one relation');
            else
              $_personsIds = array($_personsIds);
          }
          $diff = array_diff($_personsIds, $personsIds);
          if(count($diff) > 0)
            throw new Fam3InternalException('Invalid related profiles: ' . print_r($diff, true));
            
          // подготовка к сохранению  
          $relationGroup = new RelationGroup;
          foreach($_personsIds AS $_personId)
            $relationGroup->add($_personId, $relatedProfile->getAttribute('person_id'), $path);
            
          $transaction = Relation::model()->dbConnection->beginTransaction();
          try
          {
            // перед сохранением новых связей нужно удалить все связи больше 1-го порядка для $relatedProfile и каждого из $_profiles т.к. новые связи могут потенциально изменить их
            // с другой стороны надо сохранить старые связи до тех пор пока не будут вычислены новые
            // поэтому оптимальное решение - не удалять связи сразу, а пометить их как устаревшие, чтобы они продолжали использоваться сайтом, но при первой возможности были пересчитаны 
            $relationGroup->markAsDeprecated();
            
            // сохранение
            $relationGroup->save();
            
            $transaction->commit();
            
            $this->redirect($relatedProfile->getLink());
          }
          catch(Fam3BaseException $e)
          {
            throw $e;
          }
  			  catch(Exception $e)
  			  {
    			  $transaction->rollback();
            throw new Fam3InternalException($e);
  			  }
        }
        else
          $this->redirect($relatedProfile->getLink());
      }
      $this->render('relate', array('profiles' => $profiles, 'path' => $path, 'relatedProfile' => $relatedProfile));
    }
    catch(Fam3NotFoundException $e)
    {
      $this->render('error', array('exception' => $e));
    }
    catch(Fam3BaseException $e)
    {
      throw $e;
    }
    catch(Exception $e)
    {
      throw new Fam3InternalException($e);
    }
  }
  
  public function actionAdd($profileId, $relationName = null)
  {
    $this->_initProfile($profileId);
    
    if(!$this->profile->hasTheSameTree(User::instance()->getProfile()))
      throw new Fam3AccessDeniedException;
      
    $this->page = "profile-add";
    
    $this->_validateGender();
    if(is_null($relationName))
    {
      // step 1
      $this->render('addStep1', array(
        'hasFather' => $this->profile->hasParent(Gender::MALE),
        'hasMother' => $this->profile->hasParent(Gender::FEMALE),
      ));
    }
    else
    {
      // step 2
      switch($relationName)
      {
        case 'father':
          $gender = Gender::MALE;
          $relationPath = Relation::PARENT;
          break;
        case 'mother':
          $gender = Gender::FEMALE;
          $relationPath = Relation::PARENT;
          break;
        case 'daughter':
          $gender = Gender::FEMALE;
          $relationPath = Relation::CHILD;
          break;
        case 'son':
          $gender = Gender::MALE;
          $relationPath = Relation::CHILD;
          break;
        default:
          throw new Fam3InternalException('Undefined relation name: ' . $relationName);
      }
      // теперь надо проверить вдруг у $this->profile уже есть мать или отец
      if($relationPath == Relation::PARENT && $this->profile->hasParent($gender))
        throw new Fam3Exception('User already have ' . $relationName);
      
      $profile = new Profile('insert');
      
      $profile->person = new Person('insert');
      $profile->person->setAttribute('gender', $gender);
      $profile->person->setAttribute('created_by', Yii::app()->user->getId());
      $profile->person->setAttribute('tree_id', $this->profile->person->getAttribute('tree_id'));
      
      $relation = new Relation;
      $relation->setAttribute('path', $relationPath);
      $relation->setPerson1($this->profile->person);
      $relation->setPerson2($profile->person);
      
      if(isset($_POST['Profile']))
  		{
  			$profile->attributes = $_POST['Profile'];
  			if($profile->validate())
  			{
  			  $transaction = Profile::model()->dbConnection->beginTransaction();
  			  try
  			  {
  			    $profile->setAttribute('created_by', Yii::app()->user->getId());
  			    $profile->person->save(false, array('gender','created_by', 'tree_id'));
  			    $relation->setPerson2($profile->person);
  			    $profile->setAttribute('person_id', $profile->person->getPrimaryKey());
  			    $profile->save(false);
  			    Feed::createSystemProfileAdd($profile->getPrimaryKey());
  			    $relation->save(false);
  			    $relationGroup = new RelationGroup;
  			    $relationGroup->addRelation($relation);
  			    $relationGroup->save();
  			    $profile->person->setAttribute('profile_id', $profile->getPrimaryKey());
  			    $profile->person->save(false, array('profile_id'));
  			    
    			  $transaction->commit();
    			  
    			  // redirect to auto relate
    			  $this->redirect($this->profile->getLink('relate', array('relatedProfileId' => $profile->getPrimaryKey())));
  			  }
  			  catch(Fam3BaseException $e)
  			  {
    			  throw $e;
  			  }
  			  catch(Exception $e)
  			  {
    			  $transaction->rollback();
            throw new Fam3InternalException($e);
  			  }
  			}
      }
      $this->render('addStep2', array('profile' => $profile, 'relation' => $relation));
    }
  }
  
  public function actionSetGender($profileId)
  {
    $profile = $this->_initProfile($profileId);
    if($profile->hasChilds())
      $this->redirect($profile->getLink());
    if(isset($_POST['Gender']))
    {
      if(!is_array($_POST['Gender']))
        throw new Fam3InternalException('Gender must be an array');
      if(isset($_POST['Gender'][Gender::MALE]))
        $gender = Gender::MALE;
      elseif(isset($_POST['Gender'][Gender::FEMALE]))
        $gender = Gender::FEMALE;
      else
        throw new Fam3InternalException('Gender must be male or female');
      $profile->setGender($gender);
      $profile->person->save(false, array('gender'));
      $this->redirect(isset($_POST['nextAction']) ? $_POST['nextAction'] : $profile->getLink());
    }
    $this->render('setGender');
  }
  
  private function _validateGender()
  {
    if(!Gender::isValid($this->profile->getGender()))
    {
      $this->render('setGender', array('title' => 'Вы должны сперва указать ' . ($this->profile->isMy() ? 'свой ' : '') . 'пол', 'nextAction' => $this->createUrl($this->getRoute(), $this->getActionParams())));
      Yii::app()->end();
    }
  }
  
}