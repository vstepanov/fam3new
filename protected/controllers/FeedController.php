<?php

class FeedController extends Controller
{
  public $layout = 'main';
  public $page = 'feed';
  
  public function actionIndex()
  {
    $this->checkAuth();
    $profile = User::instance()->getProfile();
    $feedReader = new FeedReader($profile, FeedReader::TYPE_INDEX);
    $feedReader->load();
    $this->render('index', array('feedReader' => $feedReader));
  }
  
  public function actionProfile($profileId)
  {
    $this->checkAuth();
    $profile = $this->_initProfile($profileId);
    $feedReader = new FeedReader($profile, FeedReader::TYPE_PUBLIC);
    $feedReader->load();
    $this->render('index', array('feedReader' => $feedReader));
  }
}