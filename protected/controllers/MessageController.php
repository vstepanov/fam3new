<?php

class MessageController extends Controller
{
  public $layout = 'main';
  public $page = 'messages';
  
  public function actionIndex()
  {
    $this->checkAuth();
    $this->_loadAndRender(User::instance()->getProfile());
  }
  
  public function actionProfile($profileId)
  {
    $this->checkAuth();
    $profile = $this->_initProfile($profileId);
    if(isset($_POST['Post']))
      $this->_saveFeed($profile, $_POST['Post']);
    $this->_loadAndRender($profile);
  }
  
  private function _loadAndRender($profile)
  {
    $feedReader = new FeedReader($profile, FeedReader::TYPE_PRIVATE);
    $feedReader->load();
    $this->render('index', array('feedReader' => $feedReader, 'contractorsByPath' => User::instance()->getProfile()->getRelatives(Profile::ALL_RELATIONS, Profile::FORCE_LOAD_RELATIVES, Profile::HAVING_USERS)));
  }
}