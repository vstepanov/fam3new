<?php

class SettingController extends Controller
{
  public $page = 'settings';
  
  public function actionIndex()
  {
    if(Yii::app()->user->isGuest)
      throw new Fam3LoginRequiredException;
  	$this->render('index');
  }
}