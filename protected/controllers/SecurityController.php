<?php

class SecurityController extends Controller
{
  public $layout = 'security';
  
  public function filters()
  {
    return array(
      'redirectChecker - signout, auth, endpoint',
    );
  }
  
  public function filterRedirectChecker($filterChain)
  {
    if(!Yii::app()->user->isGuest)
      $this->redirect(Yii::app()->user->getLink());
    else
      $filterChain->run();
  }
  
  public function actionSignin()
  {
    $this->page = "signin";
    $model = new SigninForm;
    if(isset($_POST['SigninForm']) || isset($_POST['email'], $_POST['password']))
		{
		  if(isset($_POST['SigninForm']))
			  $model->attributes = $_POST['SigninForm'];
			else
		    $model->setAttributes(array('email' => $_POST['email'], 'password' => $_POST['password']));
			if($model->validate())
				$this->redirect(Yii::app()->user->returnUrl);
		}
  	$this->render('signin', array('model' => $model));
  }
  
  public function actionSignup()
  {
    $this->page = "signup";
    $user = new User('signup');
    if(isset($_POST['User']) || isset($_POST['email'], $_POST['password']))
		{
		  if(isset($_POST['User']))
			  $user->attributes = $_POST['User'];
		  else
		    $user->setAttributes(array('email' => $_POST['email'], 'password' => $_POST['password']));
			if($user->validate() && $user->createNewUser())
			{
  			Yii::app()->user->loginAsUser($user);
  			$this->redirect($user->getLink());
			}
    }
  	$this->render('signup', array('model' => $user));
  }
  
  public function actionSignout()
	{
	  $this->page = "signout";
  	Yii::app()->user->logout();
  	$this->redirect(array('default/index'));
	}
	
	public function actionEndpoint()
	{
  	Hybrid_Endpoint::process();
	}
	
	public function actionAuth($provider)
	{
  	//
	}
	
	protected function _performAjaxValidation($model)
  {
    if(isset($_POST['ajax']) && in_array($_POST['ajax'], array('signin-form', 'signup-form')))
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}