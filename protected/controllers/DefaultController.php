<?php

class DefaultController extends Controller
{
  
  public function actionIndex()
  {
  	if(Yii::app()->user->isGuest)
  	  return $this->forward('default/homepage');
		else
		  return $this->redirect(Profile::url(Yii::app()->user->getProfileId()));
  }
  
  /**
	 * This is the action to handle external exceptions.
	 */
  public function actionError()
  {
    if($error = Yii::app()->errorHandler->error)
      $this->render('error', $error);
  }
  
  public function actionHomepage()
  {
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/homepage.css');
    $this->layout = '_base';
    $this->render('homepage');
  }
}