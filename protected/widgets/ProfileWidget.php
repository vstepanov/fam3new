<?php

class ProfileWidget extends CWidget
{
  public $profile; // профиль 
  public $orientation = 'horizontal'; // ориентация профиля (горизонтальная/вертикальная)
  public $showActions = true; // отображать ссылки на редактирование профиля и тп
  
  public $relation = null; // связь между текущем профилем и пользователем
  
  public function init()
  {
    if(!Yii::app()->user->isGuest && !$this->profile->isMy())
      $this->relation = Relation::between(User::instance()->getRelated('person'), $this->profile);
  }
  
  public function run()
  {
    $this->render('profile/index', array('profile' => $this->profile, 'relationName' => ($this->relation ? $this->relation->getName(false) : null)));
  }
  
  public function renderLiveDates()
  {
    $birthDate = $this->_composeDate(
      $this->profile->birth_day,
      $this->profile->birth_month,
      $this->profile->birth_year
    );
    $deathDate = $this->_composeDate(
      $this->profile->death_day,
      $this->profile->death_month,
      $this->profile->death_year
    );
    if(!$birthDate && !$deathDate && !$this->profile->isMy()) return 'дата рождения неизвестна';
    return ($birthDate ? $birthDate : 'дата рождения') .
           ' -'.
           ($deathDate ? ' '.$deathDate : '.');
  }
  
  public function renderPlaces()
  {
    $birthPlace = $this->_composePlace($this->profile->birth_place, $this->profile->birth_country);
    $livePlace  = $this->_composePlace($this->profile->live_place,  $this->profile->live_country);
    $deathPlace = $this->_composePlace($this->profile->death_place, $this->profile->death_country);
    $places = $birthPlace .
              ($livePlace && !$deathPlace ? '&rarr;'.$livePlace : '') .
              ($deathPlace ? '&rarr;'.$deathPlace : '');
    return $places ? $places : ($this->profile->isMy() ? 'Город, Страна' : '');
  }
  
  private function _composeDate($day, $month, $year)
  {
    $date = $day ? sprintf("%02d", $day) : '';
    if($month) $date .= ($date ? '.' : '') . sprintf("%02d", $month);
    if($year) $date .= ($date ? '.' : '') . $year;
    return $date;
  }
  
  private function _composePlace($place, $country)
  {
    $_place = $place;
    if($country) $_place .= ($_place ? ', ' : '') . $country;
    return $_place;
  }
}