<?php

class RelativesWidget extends CWidget
{
  public $profile;
  
  public $relatives = null;
  
  public $showMessageEmpty = true; // отображать сообщение "Родственники не найдены"
  
  public $linkHandler = null; // обрабатывать ссылки с помощью анонимной функции переданной в $linkHandler
  
  public $showGroupCountProfiles = true; // показывать кол-во профилей в группе
  
  public function init()
  {
    if(Yii::app()->user->isGuest)
      throw new Fam3LoginRequiredException;
    if(is_null($this->relatives) && $this->profile->hasTheSameTree(User::instance()->getProfile()))
      $this->profile->loadRelatives(Profile::ALL_RELATIONS);
  }
 
  public function run()
  {
    if(!$this->profile->hasTheSameTree(User::instance()->getProfile()))
      echo 'Список родственников недоступен';
    else
      $this->render('relatives/index', array('relatives' => is_null($this->relatives) ? $this->profile->getRelatives() : $this->relatives));
  }
}