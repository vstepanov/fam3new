<div class="subwrapper">
  <div class="relatives">
  
    <?php if(count($relatives) == 0 && $this->showMessageEmpty):?>
    
      Родственники неизвестны
      
    <?php elseif(count($relatives) > 0):?>
    
      <?php foreach($relatives AS $relationPath => $profiles):?>
        <div class="relation_group">
          <h6 class="group_name<?php if(!Yii::app()->user->isGuest && isset($profiles[Yii::app()->user->getProfileId()])) echo ' is_my'?>"><?php echo Relation::name($relationPath)?><?php if($this->showGroupCountProfiles):?> (<?php echo count($profiles)?>)<?php endif ?></h6>
          <ul>
            <?php foreach($profiles AS $profile):?>
              <li<?php if(Profile::hasInstance() && Profile::instance()->equals($profile)):?> class="current"<?php endif ?>><?php echo is_null($this->linkHandler) ? $profile->link() : $this->linkHandler->__invoke($profile)?></li>
            <?php endforeach ?>
          </ul>
        </div>
      <?php endforeach ?>
    
    <?php endif ?>

  </div><!--/relatives-->
</div><!--/subwrapper-->