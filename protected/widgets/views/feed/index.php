<div id="feed" class="<?php if($feedReader->getCount() == 0) echo ' empty'?>">

  <?php if($canWrite):?>
    <div id="write_post" class="post <?php if($feedReader->typeIs(FeedReader::TYPE_PRIVATE)) echo 'private'?>">
      <form method="post" class="body">
        <textarea name="Post[body]" class="width-100" placeholder="Напишите здесь"></textarea>
        <div class="more">
          <input type="submit" class="btn" value="Написать">
          <!--<span class="privacy">
            <span class="title">Кто сможет прочитать:</span>
            <select name="Post[privacy]">
              <option value="<?php echo Feed::PRIVACY_PUBLIC?>" <?php if($feedReader->isPrivate()):?>selected="selected"<?php endif ?>>Все родственники</option>
              <?php if(!$this->profile->isMy() && $this->profile->hasUser()):?>
                <option value="<?php echo Feed::PRIVACY_PRIVATE?>" <?php if($feedReader->isPrivate()):?>selected="selected"<?php endif ?>><?php echo $this->profile->displayName()?></option>
              <?php endif ?>
            </select>
          </span>-->
        </div>
      </form>
    </div><!-- /write_post -->
  <?php endif ?>

  <?php foreach($feedReader->getFeeds() AS $feed):?>
    <div class="post <?php echo ($feed->isPrivate() ? 'private' : '') . ($feed->isSystem() ? 'system' : '')?>">
      <div class="title">
        
        <?php if($feedReader->typeIs(FeedReader::TYPE_PRIVATE) && !Profile::hasInstance()):?>
          <div class="contractor">
            <?php echo $feed->getContractor()->alink(null, 'feed', array('type' => FeedReader::TYPE_PRIVATE))?>
            <a href="<?php echo Yii::app()->createUrl('message/profile', array('profileId' => $feed->getContractor()->getPrimaryKey()))?>" class="btn open">Открыть</a>
          </div><!-- /contractor -->
        <?php else:?>
          <div class="author">
            <?php echo $feed->getRelated('author')->alink()?>
          </div><!-- /author -->
        <?php endif ?>
        <?php if($feedReader->typeIs(FeedReader::TYPE_SELF) && !$feed->isHimself()):?>
          <div class="recipient">
            <i>&rarr;</i>
            <?php echo $feed->getRelated('recipient')->arlink()?>
          </div><!-- /recipient -->
        <?php endif ?>
      </div><!-- /title -->
      <div class="body"><?php echo $feed->renderBody() ?></div>
      <div class="dt"><?php echo $feed->getAttribute('created_at')?></div>
    </div><!-- /post -->
  <?php endforeach ?>
</div><!-- /feed -->

<?php if($feedReader->getCount() == 0):?>
  <br>
  <p class="subwrapper">
    <?php if($feedReader->typeIs(FeedReader::TYPE_SELF)):?>
      Пока тут пусто
    <?php elseif($feedReader->typeIs(FeedReader::TYPE_PRIVATE)):?>
      <?php if($this->profile && $this->profile->isMy()):?>
        У вас нет личных сообщений
      <?php else:?>
        Вы не начинали переписку
      <?php endif ?>
    <?php else:?>
      Пока никто не написал здесь
    <?php endif ?>
  </p><!-- /subwrapper -->
<?php endif ?>