<?php $this->beginClip('profile');?>
  <div id="profile" <?php if(!$this->profile->hasUser()):?>class="without_user"<?php endif ?>>
    <a href="<?php echo $this->profile->getLink()?>"><img src="/img/noavatar.gif" width="150" height="150" class=" mainimage"></a>
    <div class="info">
      <h1 class="name"><a href=""><?php echo $profile->displayName() ?></a></h1>
      <?php if($relationName):?><span class="relation"><?php echo $relationName ?></span>
      <?php elseif($profile->isMy()):?><span class="relation is_my">это Вы</span>
      <?php elseif($this->profile->hasTheSameTree(User::instance()->getProfile())):?><span class="relation">Ваш родственник</span><?php endif ?>
      <?php if($liveDates = $this->renderLiveDates()): ?><div><?php echo $liveDates ?></div><?php endif ?>
      <?php  if($places = $this->renderPlaces()): ?><div class="places"><?php echo $places ?></div><?php endif ?>
    </div>
  </div><!--/profile-->
<?php $this->endClip(); ?>

<?php if($this->showActions):?>

  <div class="profile_and_actions_container row container subwrapper" <?php /*style="position:relative;"*/ ?>>
    <div class="threequarter profile_container">
      <?php echo $this->getController()->clips['profile'] ?>
    </div><!-- /threequarter profile_container -->
    <div class="quarter actions_container">
      <div id="actions">
        <ul>
          <?php /*if(!$this->profile->hasUser() && $creator = User::retrieve($this->profile->getAttribute('created_by'))->getProfile()):?>
            <li>
              Профиль создал<?php echo $creator->isFemale() ? 'а' : ''?>:<br>
              <?php echo $creator->alink()?>
            </li>
          <?php endif */?>
          <?php if($this->profile->hasTheSameTree(User::instance()->getProfile())):?>
            <?php if(!$this->profile->hasAnotherUser()):?>
              <li><a href="<?php echo $this->profile->getLink('edit')?>" class="btn <?php if($this->getController()->pageIs('profile-edit')):?>btn-active<?php endif ?>">Редактировать профиль</a></li>
            <?php endif ?>
            <li><a href="<?php echo $this->profile->getLink('add')?>" class="btn <?php if($this->getController()->pageIs('profile-add')):?>btn-active<?php endif ?>">Добавить родственника</a></li>
          <?php endif ?>
        </ul>
      </div><!-- /actions -->
    </div><!-- /quarter actions_container -->
    <?php /*<div style="position:absolute; right:0;bottom:0;"><div class="subwrapper">created by Vitaly stepanov</div></div>*/ ?>
  </div><!--/profile_and_actions_container-->

<?php else: ?>
  <div class="profile_container"><?php echo $this->getController()->clips['profile'] ?></div>
<?php endif ?>