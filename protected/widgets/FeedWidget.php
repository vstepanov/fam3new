<?php

class FeedWidget extends CWidget
{
  public $feedReader; // объект типа FeedReader, обязательный параметр
  public $profile = null; // равно Profile::instance()
  
  private $_canWrite;

  public function init()
  {
    if(Profile::hasInstance())
      $this->profile = Profile::instance();
    
    $this->_canWrite =    Profile::hasInstance()
                      && !$this->feedReader->typeIs(FeedReader::TYPE_SELF)
                      && !Yii::app()->user->isGuest
                      && !($this->feedReader->typeIs(FeedReader::TYPE_PRIVATE)
                      && $this->profile->isMy());
  }
  
  public function run()
  {
    $this->render('feed/index', array('feedReader' => $this->feedReader, 'canWrite' => $this->_canWrite));
  }
}