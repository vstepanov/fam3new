<?php

class GraphCommand extends Command
{
  private $_relations = null;
  private $_related = null;
  
  private $_analyzedPathCount;

  public function actionIndex()
  {
    $this->writeln('this is graph');
    
    return 0;
  }
  
  /**
   * По всему дереву помечает все связи >1 порядка как deprecated
   */
  public function actionRenew($treeId)
  {
    $tree = $this->_initTree($treeId);
    $relationGroup = new RelationGroup;
    $relationGroup->loadAllRelationsByTree($tree);
    $this->writeln('changed: ' . $relationGroup->markAsDeprecated());
  }
  
  public function actionComplete($treeId, $insert = null, $printPath = null)
  {
    $this->addOpt('insert', $insert);
    $this->addOpt('printPath', $printPath);
    
    $tree = $this->_initTree($treeId);
    
  	$relationCount = $this->_loadRelations($treeId);
  	
  	$sql = 'select pe.id, pe.gender, pr.name, pr.surname from ' . Person::tableName() . ' pe left join ' . Profile::tableName() . ' pr on (pr.id = pe.profile_id) where tree_id = :tree_id';
  	$command = Profile::model()->dbConnection->createCommand($sql);
  	$command->bindParam(":tree_id", $treeId, PDO::PARAM_INT);
  	$dataReader = $command->query();
  	$dataReader->setFetchMode(PDO::FETCH_NUM);
  	$persons = array();
  	$profiles = array();
  	while(($person = $dataReader->read()) !== false)
  	{
  	  $persons[$person[0]] = $person[1];
  	  $profileName = $person[2];
  	  if($person[3])
  	    $profileName .= ($profileName ? ' ' : '') . $person[3];
  	  if(!$profileName)
  	    $profileName = 'noname ' . $person[0];
  	  $profiles[$person[0]] = $profileName;
    }
  	$personCount = count($persons);
  	$undefinedRelationCount = $personCount * ($personCount - 1) - $relationCount;
  	
  	$this->writeln('person count: ' . $personCount);
  	$this->writeln('relation count: ' . $relationCount);
  	$this->writeln('undefined relation count: ' . $undefinedRelationCount);
  	$this->writeln();
  	$this->writeln('relations:');
  	foreach($this->_relations AS $person1 => $_relations)
  	{
  	  foreach($_relations AS $person2 => $path)
  	  {
  	    $relationName = Relation::name($path, $persons[$person2], $persons[$person1], false);
        $this->writeln(Utility::mb_str_pad($person1 . ' -> ' . $person2, 20) . "\t" . Utility::mb_str_pad($this->_getBinPath($path), 20) . "\t" . Utility::mb_str_pad($relationName, 20) . "\t" . Utility::mb_str_pad($profiles[$person1], 30) . "\t" . Utility::mb_str_pad($profiles[$person2], 30));
      }
  	}
  	if($undefinedRelationCount > 0)
  	{
    	$this->writeln();
    	$this->writeln('find undefined relations:');
    	$undefinedRelationIterator = 0;
    	if($this->opt('insert'))
        $insertRelations = array();
      $this->_analyzedPathCount = 0;
    	foreach($persons AS $person1 => $gender1)
    	{
      	foreach($persons AS $person2 => $gender2)
      	{
        	if($person1 == $person2) continue;
        	if(!isset($this->_relations[$person1][$person2]))
        	{
          	$undefinedRelationIterator++;
          	$path = $this->_searchPath($person1, $person2);
          	if($this->opt('insert'))
          	  $insertRelations[] = array($person1, $person2, $path);
          	$relationName = Relation::name($path, $persons[$person2], $persons[$person1], false);
          	$this->writeln(Utility::mb_str_pad($undefinedRelationIterator . '/' . $undefinedRelationCount . ' ' . $person1 . ' -> ' . $person2, 20) . "\t" . Utility::mb_str_pad($this->_getBinPath($path), 20) . "\t" . Utility::mb_str_pad($relationName, 20) . "\t" . Utility::mb_str_pad($profiles[$person1], 30) . "\t" . Utility::mb_str_pad($profiles[$person2], 30));        	}
      	}
    	}
    	$this->writeln('analyzed path count: ' . $this->_analyzedPathCount);
    	if($this->opt('insert'))
      {
        $this->writeln();
        $this->write('save...');
        $relationGroup = new RelationGroup($insertRelations);
        try
        {
          $changedCount = $relationGroup->save();
          $this->writeln('OK');
        }
        catch(Fam3BaseException $e)
        {
          throw $e;
        }
        catch(Exception $e)
        {
          $this->write('Fail');
          throw new Fam3InternalException($e);
        }
        $this->writeln('changed count: ' . $changedCount);
      }
    }
  }
  
  public function getHelp()
  {
    return parent::getHelp() . "\r\n";
  }
  
  private function _loadRelations($treeId)
  { 
  	$sql = '
select
	r.person1_id, r.person2_id, path
from ' . Relation::tableName() . ' r
left join person p on (r.person1_id = p.id or r.person2_id = p.id)
where p.tree_id = :tree_id and r.deprecated = 0
group by r.person1_id, r.person2_id';
  	$command = Relation::model()->dbConnection->createCommand($sql);
  	$command->bindParam(":tree_id", $treeId, PDO::PARAM_INT);
  	$dataReader = $command->query();
  	$dataReader->setFetchMode(PDO::FETCH_NUM);
  	$this->_relations = array();
  	$this->_related = array();
  	$relationCount = 0;
  	while(($relation = $dataReader->read()) !== false)
  	{
  	  $person1 = $relation[0];
  	  $person2 = $relation[1];
  	  $path = $relation[2];
  	  if(!isset($this->_relations[$person1])) $this->_relations[$person1] = array();
    	$this->_relations[$person1][$person2] = $path;
    	$relationCount++;
    	if(Relation::isFirstLevel($path))
    	{
    	  if(!isset($this->_related[$person1])) $this->_related[$person1] = array();
    	  if(!isset($this->_related[$person2])) $this->_related[$person2] = array();
    	  $this->_related[$person1][$person2] = ($path == Relation::PARENT ? 0 : 1);
    	  $this->_related[$person2][$person1] = ($path == Relation::PARENT ? 1 : 0);
      }
  	}
  	return $relationCount;
  }
  
  private function _searchPath($person1, $person2)
  {
    $path = $this->_doPath(array($person1), $person2);
    if(!$path)
      throw new Fam3InternalException('Can\'t find path between person ' . $person1 . ' and person ' . $person2);
    if($this->opt('printPath'))
    {
      $this->write('path: ');
      $this->_printPath($path);
    }
    $binPath = '1';
    for($i = 0; $i < count($path) - 1; $i++)
      $binPath .= $this->_related[$path[$i]][$path[$i + 1]];
    return bindec($binPath);
  }
  
  private function _doPath($path, $targetPerson)
  {
    $this->_analyzedPathCount++;
    $this->_printPath($path);
    $paths = array();
    foreach($this->_related[$path[count($path) - 1]] AS $person => $p)
    {
      if(in_array($person, $path))
        continue;
      $newPath = $path;
      $newPath[] = $person;
      if($person == $targetPerson)
        return $this->_printPath($newPath);
      else
      {
        $result = $this->_doPath($newPath, $targetPerson);
        if($result)
          $paths[count($result)] = $result;
      }
    }
    if(count($paths) == 0)
      return false;
    ksort($paths);
    return current($paths);
  }

  private function _printPath($path)
  {
    if($this->opt('printPath'))
    {
      $str = '';
      foreach($path AS $p)
      {
        $str .= ($str ? '-' : '') . $p;
      }
      $this->writeln("\t".$str);
    }
    return $path;
  }
  
  private function _getBinPath($path)
  {
    return '0b' . decbin($path);
  }
  
  private function _initTree($treeId)
  {
    $tree = Tree::model()->findByPk($treeId);
    if(!$tree) throw new Fam3InternalException('Tree not found id' . $treeId);
    $this->writeln('tree: ' . $tree['id'] . ' @ ' . $tree['created_at']);
    return $tree;
  }
  
}